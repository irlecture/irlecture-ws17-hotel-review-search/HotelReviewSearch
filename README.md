# Hotel Review Search

## About

_Hotel Review Search_ is a high-quality search engine built on top of Apache Lucene. Using a huge corpus of 
hotel reviews from the online traveling platform TripAdvisor, this application offers a fast and user 
friendly access to thousands of real-world hotel ratings. The integrated web application has a user interface to
specify query terms and view the matching hotels and its reviews. After selecting a hotel, a detail page with its available
properties is shown together with a list of all reviews of this hotel, ordered by relevance to the given query.
Possible queries are: ``vegetarian food``, ``pet friendly hotel in berlin``, ``air conditioning``

## Benefits

* Interactive suggestions in the search bar from query-logs and term analysis
* Detection and processing of hotel location terms in the query
* Easy change of the retrieval model with a single config parameter
* Highlight of query terms in all result pages

## Requirements

* Apache Maven build and dependency management tool
    * tested with version 3.3.9
* Java 
    * tested with version 1.8
* Git 

## Cloning the Git repository
* run `git clone https://git.informatik.uni-leipzig.de/irlecture/group-b/HotelReviewSearch.git`

## Install

* go to project root (HotelReviewSearch/) and create a jar file using maven
    * run `mvn package`
    * maven should be packaging this per default to the target/ directory (result-filename: HotelReviewSearch-1.0-SNAPSHOT.jar)
    * copy this jar file to any folder you want
    * in this folder, create a new folder structure 'data/normalized'
    * copy the document corpus from data/normalized into this new structure
    * create a folder structure 'data/evaluation/topics' in the directory of the jar file
    * copy the evaluation topics from the identically named source path to this directory

## Usage

* starting the application
    * run the previously created jar file
    * run `java -jar path/to/jar-file`
        * a help is shown how to use the file
        * param: index = create the indexes from the corpus
        * param: webapp = start the web application
        * param: eval = create relevance judgement files from the evaluation topics

1. create the indexes
    * run `java -jar path/to/jar-file index`
2. run the web application
    * run `java -jar path/to/jar-file webapp`
    * the application now should be started (you should see the "Spring" logo)
    * navigate with your web browser to the url http://http://localhost:8080/
    * stop the application:
        * press 'CTRL + C' to stop the web application
3. evaluation:
    * create relevance judgement files:
    * run `java -jar path/to/jar-file eval`
    * the files appear in the /data/evaluation/relevances/ directory
    * manually evaluate the topics (1 = relevant, 0 = irrelevant, -1 = not rated jet)
    * calculate precission and recall:
    * again run `java -jar path/to/jar-file eval`


## The directory structure of the sources

### Evaluation
* Topics
    * \data\evaluation\topics
* Relevance Judgments:    
    * first judgment:
        * \data\evaluation\relevances_20180108
    * second judgment after optimization:
        * \data\evaluation\relevances

### Corpus
* Original Corpus as provided by Bauhaus-Universität Weimar
    * \data\original
* Normalized Corpus, extended with Cities detected by GeoText
    * \data\normlaized
* files created during the analyzation of the corpus, showing which different values occur in the json fields
    * \data\user_values

### Java executable Main Classes
* \src\main\java\com\hotelReviewSearch
* Start the Web Application
    * .\Main.java
* Start the Indexing Process 
    * .\IndexMain.java
* Start the Evaluation Process
    * .\EvaluationMain.java
* Several tests of possible optimizations
    * TestGeoTextMain.java (discarded)
    * TestNamedEntityRecognitionMain.java (discarded)
    * TestSentimentalMain.java (discarded)
    * TestLocationFromIndexMain.java (implemented)