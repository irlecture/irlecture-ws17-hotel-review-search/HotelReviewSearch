#!/usr/bin/python

import json
import os
from geotext import GeoText
from collections import Counter
import requests
import bs4 as bs
import configparser
import logging
import time

config = configparser.ConfigParser()
cwd = os.getcwd()
config.read(os.path.join(cwd, './config/config.ini'))

DELAY = float(config['http-request']['delay'])
USER_AGENT = config['http-request']['user-agent']
URL_TA = config['http-request']['url-tripadvisor']

logger = logging.getLogger('hrs_data')
hdlr = logging.FileHandler(config['log']['log-file'])
formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s', datefmt='%d.%m.%Y %H:%M:%S')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)


def get_places(parts):
    """
    Searches for cities within parts.
    :param parts: Either a string or a list
    :type parts: str() or list()
    :return: list with places or empty list
    """
    tmp = list()
    if parts:
        if type(parts) == str:
            parts = str(parts).split()
        for element in parts:
            places = GeoText(element).cities
            if places:
                tmp.extend([place for place in places if place not in tmp])
    return tmp


def add_places_from_keys(hotel_dict):
    """
    Adds geo-text information to places key of hotel_dict.
    :param hotel_dict: represents a hotel
    :type hotel_dict: dict()
    :return: hotel_dict
    """
    if hotel_dict:
        hotel_dict['places']['hotel_name'].extend(get_places(hotel_dict['name']))
        hotel_dict['places']['hotel_description'].extend(get_places(hotel_dict['description']))
        hotel_dict['places']['aka'].extend(get_places(hotel_dict['aka']))
    return hotel_dict


def normalize_origin_hotel_json(hotel_dict):
    """
    Adds all missing parameters (keys) referring to hotel-meta defined within json-format file (normalization).
    Additionally some new keys like 'places', 'tripadvisor' and 'proposed_city'.
    This should sets all hotel_dicts to exactly the same structure.
    :param hotel_dict: represents a hotel
    :type hotel_dict: dict()
    :return: hotel_dict
    """
    # normalization
    hotel_dict['name'] = hotel_dict['name'] if 'name' in hotel_dict else None

    hotel_dict['location'] = hotel_dict['location'] if 'location' in hotel_dict else dict()
    hotel_dict['location']['continent'] = hotel_dict['location']['continent'] \
        if 'continent' in hotel_dict['location'] else None
    hotel_dict['location']['country'] = hotel_dict['location']['country'] \
        if 'country' in hotel_dict['location'] else None
    hotel_dict['location']['province'] = hotel_dict['location']['province'] \
        if 'province' in hotel_dict['location'] else None
    hotel_dict['location']['region'] = hotel_dict['location']['region'] \
        if 'region' in hotel_dict['location'] else None
    hotel_dict['location']['city'] = hotel_dict['location']['city'] \
        if 'city' in hotel_dict['location'] else None
    hotel_dict['location']['proposed_city'] = hotel_dict['location']['proposed_city'] \
        if 'proposed_city' in hotel_dict['location'] else None

    hotel_dict['slim_ranking'] = hotel_dict['slim_ranking'] if 'slim_ranking' in hotel_dict else None
    hotel_dict['user_rating'] = hotel_dict['user_rating'] if 'user_rating' in hotel_dict else None
    hotel_dict['review_count'] = hotel_dict['review_count'] if 'review_count' in hotel_dict else None

    hotel_dict['detail_rating'] = hotel_dict['detail_rating'] if 'detail_rating' in hotel_dict else dict()
    hotel_dict['detail_rating']['location'] = hotel_dict['detail_rating']['location'] \
        if 'location' in hotel_dict['detail_rating'] else None
    hotel_dict['detail_rating']['sleep'] = hotel_dict['detail_rating']['sleep'] \
        if 'sleep' in hotel_dict['detail_rating'] else None
    hotel_dict['detail_rating']['rooms'] = hotel_dict['detail_rating']['rooms'] \
        if 'rooms' in hotel_dict['detail_rating'] else None
    hotel_dict['detail_rating']['service'] = hotel_dict['detail_rating']['service'] \
        if 'service' in hotel_dict['detail_rating'] else None
    hotel_dict['detail_rating']['value'] = hotel_dict['detail_rating']['value'] \
        if 'value' in hotel_dict['detail_rating'] else None
    hotel_dict['detail_rating']['cleanliness'] = hotel_dict['detail_rating']['cleanliness'] \
        if 'cleanliness' in hotel_dict['detail_rating'] else None

    hotel_dict['variance'] = hotel_dict['variance'] if 'variance' in hotel_dict else dict()
    hotel_dict['variance']['excellent'] = hotel_dict['variance']['excellent'] \
        if 'excellent' in hotel_dict['variance'] else None
    hotel_dict['variance']['very good'] = hotel_dict['variance']['very good'] \
        if 'very good' in hotel_dict['variance'] else None
    hotel_dict['variance']['average'] = hotel_dict['variance']['average'] \
        if 'average' in hotel_dict['variance'] else None
    hotel_dict['variance']['poor'] = hotel_dict['variance']['poor'] \
        if 'poor' in hotel_dict['variance'] else None
    hotel_dict['variance']['terrible'] = hotel_dict['variance']['terrible'] \
        if 'terrible' in hotel_dict['variance'] else None

    hotel_dict['aka'] = hotel_dict['aka'] if 'aka' in hotel_dict else None
    hotel_dict['stars'] = hotel_dict['stars'] if 'stars' in hotel_dict else None
    hotel_dict['description'] = hotel_dict['description'] if 'description' in hotel_dict else None

    hotel_dict['reviews'] = hotel_dict['reviews'] if 'reviews' in hotel_dict else list()
    if hotel_dict['reviews']:
        for index, review in enumerate(hotel_dict['reviews']):
            review['id'] = review['id'] if 'id' in review else None
            review['user'] = review['user'] if 'user' in review else None
            review['title'] = review['title'] if 'title' in review else None
            review['rating'] = review['rating'] if 'rating' in review else None
            review['date'] = review['date'] if 'date' in review else None
            review['text'] = review['text'] if 'text' in review else None
            review['likes'] = review['likes'] if 'likes' in review else dict()
            review['likes']['liked'] = review['likes']['liked'] if 'liked' in review['likes'] else None
            review['likes']['disliked'] = review['likes']['disliked'] if 'disliked' in review['likes'] else None
            review['stay_parameters'] = review['stay_parameters'] if 'stay_parameters' in review else None
            review['detail_rating'] = review['detail_rating'] if 'detail_rating' in review else dict()
            review['detail_rating']['value'] = review['detail_rating']['value'] \
                if 'value' in review['detail_rating'] else None
            review['detail_rating']['location'] = review['detail_rating']['location'] \
                if 'location' in review['detail_rating'] else None
            review['detail_rating']['desk'] = review['detail_rating']['desk'] \
                if 'desk' in review['detail_rating'] else None
            review['detail_rating']['rooms'] = review['detail_rating']['rooms'] \
                if 'rooms' in review['detail_rating']else None
            review['detail_rating']['cleanliness'] = review['detail_rating']['cleanliness'] \
                if 'cleanliness' in review['detail_rating'] else None
            review['detail_rating']['service'] = review['detail_rating']['service'] \
                if 'service' in review['detail_rating'] else None
            review['detail_rating']['business'] = review['detail_rating']['business'] \
                if 'business' in review['detail_rating'] else None
            review['detail_rating']['sleep'] = review['detail_rating']['sleep'] \
                if 'sleep' in review['detail_rating'] else None
            review['detail_rating']['restaurant'] = review['detail_rating']['restaurant'] \
                if 'restaurant' in review['detail_rating'] else None
            review['detail_rating']['pool'] = review['detail_rating']['pool'] \
                if 'pool' in review['detail_rating'] else None
            review['helpfulness'] = review['helpfulness'] if 'helpfulness' in review else None
            hotel_dict['reviews'][index] = review

    # additional keys
    hotel_dict['location']['longitude'] = None
    hotel_dict['location']['latitude'] = None
    hotel_dict['review_count_current'] = len(hotel_dict['reviews'])
    hotel_dict['tripadvisor'] = {
        'VISITED': False # internal
        , 'pic_link': None
        , 'street_address': None
        , 'locality': None
        , 'country_name': None
    }
    hotel_dict['places'] = {
        'hotel_name': list()
        , 'hotel_description': list()
        , 'aka': list()
    }
    return hotel_dict


def download_html(url_path, params):
    """
    Download html content.
    :param url_path: path of a url (part after host[:port])
    :param params: query parameters
    :type params: dict()
    :return: html text content
    """
    url = "%s%s" % (URL_TA, url_path)
    headers = {
        'User-Agent': USER_AGENT
    }
    r = requests.get(url=url, headers=headers, params=params)
    if r:
        r = r.text
    return r if r else None


def parse_hotel_html(html_file, hotel_dict):
    """
    Parses html for specific tag to retrieve information like title, address, locality and
    a path to a hotel photo.
    :param html_file: raw html content
    :param hotel_dict: represents a hotel
    :type hotel_dict: dict()
    :return: updated hotel_dict (key: 'tripadvisor') if found some information
    """
    soup = bs.BeautifulSoup(html_file, 'lxml')
    container = soup.find('div', {'class': 'result LODGING'})
    if container:
        title = container.find('div', {'class': 'title'}).find('span')
        sources = container.find_all('div', {'class': 'title', 'onclick': True})
        hotel_page_url_ta = ""
        if sources:
            attr = str(sources[0]['onclick'])
            hotel_page_url_ta = attr[attr.find('/Hotel_Review'):attr.rfind('\')')]
        if title:
            hotel_dict['tripadvisor']['title'] = title.text
        sized_thumb = container.find('div', {'class': 'sizedThumb'})
        sources = sized_thumb.find_all('img', {'src': True})
        if sources:
            hotel_dict['tripadvisor']['pic_link'] = sources[0]['src']
        time.sleep(DELAY)
        # link to hotel page (tripadvisor page) within title-tag attribute 'onclick'

        hotel_page_ta = download_html(hotel_page_url_ta, {})

        soup = bs.BeautifulSoup(hotel_page_ta, 'lxml')
        hotel_content = soup.find('div', {'class': 'blRow', 'data-context': 'Hotel_Review'})
        street_address = hotel_content.find('span', {'class': 'street-address'})
        if street_address:
            hotel_dict['tripadvisor']['street_address'] = street_address.text
        locality = hotel_content.find('span', {'class': 'locality'})
        if locality:
            hotel_dict['tripadvisor']['locality'] = locality.text
        country_name = hotel_content.find('span', {'class': 'country-name'})
        if country_name:
            hotel_dict['tripadvisor']['country_name'] = country_name.text
    hotel_dict['tripadvisor']['VISITED'] = True
    return hotel_dict


def search_at_tripadvisor(hotel_dict):
    """
    Downloads and parses html content from tripadvisor.com.
    Gathers some more geo-information about the hotel
    :param hotel_dict: represents a hotel
    :type hotel_dict: dict()
    :return: hotel_dict
    """
    hotel_name = hotel_dict['name']
    html_file = download_html('/Search', {'q': hotel_name})
    parse_hotel_html(html_file, hotel_dict)
    return hotel_dict


def match_city(hotel_dict):
    """
    Evaluates the keys under 'places'-key. Returns the most common place.
    :param hotel_dict: represents a hotel
    :type hotel_dict: dict()
    :return: proposed city (string) or None if nothing found
    """
    # TODO: improve evaluation...
    proposed_city = None
    all_occurrences = list()
    all_occurrences.extend(hotel_dict['places']['hotel_name'])
    all_occurrences.extend(hotel_dict['places']['hotel_description'])
    all_occurrences.extend(hotel_dict['places']['aka'])
    tmp = Counter(all_occurrences)
    most_common = tmp.most_common()

    if len(most_common) == 1:
        proposed_city = most_common[0][0]

    # wenn alle gleich: --> keine auswertung, da alle gleich geranked
    elif [entry[1] for entry in most_common][1:] == [entry[1] for entry in most_common][:-1]:
        # all cities has the same number of occurrences
        pass

    # 2 höchstgerankte ... --> keine auswertung

    # alles andere
    else:
        # check which element in list has the most occurrences
        # [('Berlin', 2), ('Leipzig', 3), ('Dresden', 1), ('Hamburg', 2)]
        m = max([e[1] for e in most_common])
        index = [i for i, x in enumerate([e[1] for e in most_common]) if x == m]
        if index:
            proposed_city = most_common[index[0]][0]
    return proposed_city


def set_proposed_city(hotel_dict):
    """
    Sets the proposed city if there is no one in original keys ('location'-->'city')
    Lookup at tripadvisor-key for the city if present or evaluates cities found bei geotext
    :param hotel_dict: represents a hotel
    :type hotel_dict: dict()
    :return: hotel_dict
    """
    if not hotel_dict['tripadvisor']['locality']:
        proposed_city = match_city(hotel_dict)
        hotel_dict['location']['proposed_city'] = proposed_city
    else:
        places_tripadvisor_locality = get_places(hotel_dict['tripadvisor']['locality'])
        # TODO: check if there are multiple places
        hotel_dict['location']['proposed_city'] = places_tripadvisor_locality
    return hotel_dict


def read_json(json_file_path):
    """
    :param json_file_path: path to json file
    :return: dict
    """
    with open(json_file_path, 'r') as fp:
        json_dict = json.load(fp)
    fp.close()
    return json_dict if json_dict else None


def write_dict(dict_to_write, json_path, pretty=False):
    """
    Writes and converts a dict to a regular JSON-file to given path.
    :param pretty: 
    :param dict_to_write: dict
    :param json_path: path where JSON-file should been saved
    """
    with open(json_path, 'w+') as file:
        if pretty:
            file.write(json.dumps(dict_to_write, indent=4, sort_keys=True))
        else:
            file.write(json.dumps(dict_to_write))
    file.close()


if __name__ == '__main__':
    # hotel_source_path = config['data']['source']
    hotel_source_path = config['data']['destination']
    hotel_destination_path = config['data']['destination']

    # count json files
    len_files = 0
    if os.path.exists(hotel_source_path):
        for root, dirs, files in os.walk(hotel_source_path, topdown=False):
            len_files = len(files)
            break

    if os.path.exists(hotel_source_path):
        current = 0
        for root, dirs, files in os.walk(hotel_source_path, topdown=False):
            for name in files:
                current += 1
                json_file_path = os.path.join(root, name)
                try:
                    hotel_dict = read_json(json_file_path)
                    # normalization should be always first, if updating keys within hotel-dict
                    hotel_dict = normalize_origin_hotel_json(hotel_dict)
                    hotel_dict = add_places_from_keys(hotel_dict)
                    if not hotel_dict['location']['city']:
                        if hotel_dict['tripadvisor']['VISITED'] is False:
                            try:
                                hotel_dict = search_at_tripadvisor(hotel_dict)
                            except Exception as e:
                                msg = "[{current}/{len_files}] ERROR while search '{hotel_name}' at tripadvisor - " \
                                      "{error}".format(current=current, len_files=len_files,
                                                       hotel_name=hotel_dict['name'], error=e)
                                logger.info(msg)
                                print(msg)
                        else:
                            msg = "[{current}/{len_files}] tripadvisor already VISITED - '{file}'" \
                                .format(current=current, len_files=len_files, file=json_file_path)
                            logger.info(msg)
                            print(msg)
                    else:
                        msg = "[{current}/{len_files}] Origin city EXISTS already - '{file}'"\
                            .format(current=current, len_files=len_files, file=json_file_path)
                        logger.info(msg)
                        print(msg)
                    hotel_dict = set_proposed_city(hotel_dict)

                    save_path = os.path.join(hotel_destination_path, name)
                    write_dict(hotel_dict, save_path, pretty=False)
                    time.sleep(DELAY)
                    msg = "[{current}/{len_files}] SUCCESS - updated '{file}'" \
                        .format(current=current, len_files=len_files, file=json_file_path)
                    logger.info(msg)
                    print(msg)
                except Exception as e:
                    msg = "[{current}/{len_files}] ERROR at '{file}' - {error}"\
                        .format(current=current, len_files=len_files, file=json_file_path, error=e)
                    logger.info(msg)
                    print(msg)
