from geotext import GeoText
import argparse


def init_argument_parser():
    """
    Initializes the arguments for this program and returns parsed arguments.
    :return: program arguments
    """
    ap = argparse.ArgumentParser()
    ap.add_argument('-q', '--query', help="search for city names", required=True)
    ap.add_argument('-c', '--cities', help="search for city names", required=False, action='store_true')
    ap.add_argument('-o', '--countries', help="search for country names", required=False, action='store_true')
    ap.add_argument('-m', '--countryMentions', help="search for country mentions", required=False, action='store_true')
    ap.add_argument('-n', '--nationalities', help="search for nationalities", required=False, action='store_true')
    ap.add_argument('-i', '--index', help="returns the index of country mentions", required=False, action='store_true')
    return ap.parse_args()


def find_geo_locations(query, cities=True, countries=True, country_mentions=True, nationalities=True):
    """
    :param query: string where geotext looks for geo locations.
    :param cities: if true geotext looks for it
    :param countries: if true geotext looks for it
    :param country_mentions: if true geotext looks for it
    :param nationalities: if true geotext looks for it
    :return: dictionary (hash-map) with geo locations (or not)
    """
    p = {
        'cities': None
        , 'countries': None
        , 'country_mentions': None
        , 'nationalities': None
    }
    places = GeoText(query)
    if cities:
        p['cities'] = places.cities
    if countries:
        p['countries'] = places.countries
    if country_mentions:
        p['country_mentions'] = dict(places.country_mentions)
    if nationalities:
        p['nationalities'] = places.nationalities
    return p


def main():
    """
    Processes the functions in the right order.
    :return: print output
    """
    args = init_argument_parser()
    query = args.query
    cities = args.cities
    countries = args.countries
    country_mentions = args.countryMentions
    nationalities = args.nationalities
    locations = find_geo_locations(query, cities, countries, country_mentions, nationalities)
    print(locations)


if __name__ == '__main__':
    main()
