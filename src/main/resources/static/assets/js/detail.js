$(function () {

    var queryString = $('#queryString').data('query');

    $.fn.stars = function() {
        return $(this).each(function() {

            // ugly bugfix for missing star when number ends on .0
            var rating = $(this).data("rating");
            if (rating == 5.0){
                rating = 5;
            } else if (rating == 4.0){
                rating = 4;
            } else if (rating == 3.0){
                rating = 3;
            } else if (rating == 2.0){
                rating = 2;
            } else if (rating == 1.0){
                rating = 1;
            }

            var numStars = $(this).data("numStars");

            var fullStar = new Array(Math.floor(rating + 1)).join('<i class="fa fa-star"></i>');

            var halfStar = ((rating%1) !== 0) ? '<i class="fa fa-star-half-empty"></i>': '';

            var noStar = new Array(Math.floor(numStars + 1 - rating)).join('<i class="fa fa-star-o"></i>');

            $(this).html(fullStar + halfStar + noStar);

        });
    }

    $('.stars').stars();

});

