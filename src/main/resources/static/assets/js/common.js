$(function () {
    $('#search-input').focus();

    // constructs the suggestion engines
    var termFrequencySuggestions = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            // cache: false => lets Bloodhound always request the remote source
            cache: false,
            url: '/REST/suggest?q=%QUERY&e=termFreq',
            wildcard: '%QUERY'
        }
    });
    var queryLogSuggestions = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            cache: false,
            url: '/REST/suggest?q=%QUERY&e=queryLog',
            wildcard: '%QUERY'
        }
    });

    // autocomplete input
    $('#search-input').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            source: termFrequencySuggestions.ttAdapter(),
            limit: 5,
            templates: {
                header: '<h5 class="league-name">Most frequent terms:</h5>'
            }
        },
        {
            source: queryLogSuggestions.ttAdapter(),
            limit: 5,
            templates: {
                header: '<hr><h5 class="league-name">Queries from other users:</h5>'
            }
        });
});