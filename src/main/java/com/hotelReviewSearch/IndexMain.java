package com.hotelReviewSearch;

import com.hotelReviewSearch.Entity.SearchRequest;
import com.hotelReviewSearch.Index.*;
import org.apache.lucene.queryparser.classic.ParseException;
import java.io.IOException;

/**
 * Main class to trigger the lucene indexing process
 * Architecture of this class - Currently this implementation of following tutorial:
 * http://www.lucenetutorial.com/lucene-in-5-minutes.html [2017-11-1]
 */
public class IndexMain
{
    /**
     * Public main method to trigger the lucene indexing process
     * @param args arguments
     * @throws IOException
     * @throws ParseException
     */
    public static void main(String[] args) throws IOException, ParseException
    {
        // Set this to false if you will not reset the index before creating it
        boolean resetIndex = true;

        // Commented out because it is the old index
        //IndexEngine indexEngine = new IndexEngine();
        //indexEngine.createIndex(resetIndex);

        ReviewIndexEngine reviewIndexEngine = new ReviewIndexEngine();
        reviewIndexEngine.createIndex(resetIndex);

        System.out.println("***");

        // Commented out because it is the old hotelIndex
        //HotelIndexEngine hotelIndexEngine = new HotelIndexEngine();
        //hotelIndexEngine.createIndex(resetIndex);

        HotelReviewIndexEngine hotelReviewIndexEngine = new HotelReviewIndexEngine();
        hotelReviewIndexEngine.createIndex(resetIndex);

        System.out.println("***");

        RequestIndexEngine requestIndexEngine = new RequestIndexEngine();
        requestIndexEngine.createIndex(resetIndex);
    }
}
