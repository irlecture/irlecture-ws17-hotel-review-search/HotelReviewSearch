package com.hotelReviewSearch.Evaluation;

/**
 * Evaluation exception if a missing feedback was detected
 */
public class FeedbackMissingException extends Exception {

    public FeedbackMissingException(String message) {
        super(message);
    }
}
