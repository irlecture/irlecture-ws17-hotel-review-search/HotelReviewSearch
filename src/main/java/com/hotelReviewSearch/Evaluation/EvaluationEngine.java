package com.hotelReviewSearch.Evaluation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hotelReviewSearch.Dao.SearchDaoInterface;
import com.hotelReviewSearch.Entity.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Class for evaluation logic
 */
public class EvaluationEngine
{
    private static final Logger logger = LogManager.getLogger(EvaluationEngine.class);

    private SearchDaoInterface _searchDao;

    private int _feedbackRelevanceCount = 0;

    public EvaluationEngine(SearchDaoInterface searchDao) {
        this._searchDao = searchDao;
    }

    /**
     * Read all topic json files from the given folder to objects and return them as list
     *
     * @param pathToJsonFiles the path to the json files, e.g. "data/evaluation/topics"
     * @return a list of topic entities
     * @throws IOException if file can't be read
     */
    public List<EvaluationTopic> getTopicsFromJsonFiles(String pathToJsonFiles) throws IOException {
        List<EvaluationTopic> topicList = new ArrayList<>();

        File folder = new File(pathToJsonFiles);
        File[] listOfFiles = folder.listFiles();

        if (listOfFiles != null) {
            for (File file : listOfFiles) {
                if (file.isFile() && file.canRead() && file.getName().startsWith("topic")) {
                    byte[] content = Files.readAllBytes(Paths.get(file.getPath()));
                    String fileContent = new String(content);   // eventuell muss hier das encoding noch angepasst werden

                    GsonBuilder gsonBuilder = new GsonBuilder();
                    gsonBuilder.serializeNulls();
                    Gson gson = gsonBuilder.create();

                    topicList.add(gson.fromJson(fileContent, EvaluationTopic.class));
                }
            }
        }

        return topicList;
    }

    /**
     * Runs a query against the search engine with the topic title as query string
     * All Hotel-Review Combinations will be put to a relevance list inside the topic
     *
     * @param topic the topic to search for
     * @return a list of relevance pairs
     * @throws Exception
     */
    public List<EvaluationRelevance> getRelevanceTemplatesByTopic(EvaluationTopic topic) throws Exception{
        List<EvaluationRelevance> relevanceList = new ArrayList<>();
        String queryString = topic.getTitle();

        Page<SearchResult> searchResultsPage = _searchDao.getSearchResultsPage(queryString,1,10);
        for (SearchResult searchResult : searchResultsPage.getItemsOnPage()) {
            int hotelId = searchResult.getHotel().getHotelId();
            Collection<ReviewEntity> topReviewsOfHotels = _searchDao.getTopReviewsOfHotel(hotelId, queryString, 10);
            for (ReviewEntity reviewEntity : topReviewsOfHotels) {
                relevanceList.add(new EvaluationRelevance(hotelId, reviewEntity.getId()));
            }
        }

        return relevanceList;
    }

    /**
     * Writes relevance files
     *
     * @param topic the topics with relevance information
     * @param relevancesPath the path to write the relevance files
     * @param overwrite if set to true, existing files will be overwritten
     * @throws FileNotFoundException
     * @throws UnsupportedEncodingException
     */
    public void writeRelevances(EvaluationTopic topic, String relevancesPath, boolean overwrite) throws FileNotFoundException, UnsupportedEncodingException {
        File folder = new File(relevancesPath);

        if (!folder.exists() || !folder.isDirectory()) {
            boolean result = folder.mkdir();
            if (!result) {
                logger.error("Can not create directory [" + relevancesPath + "].");
                return;
            }
        }

        Gson gson = (new GsonBuilder()).serializeNulls().setPrettyPrinting().create();

        String fileName = relevancesPath + "/relevance_" + topic.getId() + ".json";

        File file = new File(fileName);

        if (!file.exists() || overwrite) {
            String topicJsonString = gson.toJson(topic, EvaluationTopic.class);
            PrintWriter writer = new PrintWriter(fileName, "UTF-8");
            writer.print(topicJsonString);
            writer.close();
            logger.info("Relevance file [" + fileName + "] created.");
        }
    }

    /**
     * Updated the relevance file with missing entires
     *
     * @param topic
     * @param relevanceFileName
     * @return true if the file was updated, false if not
     * @throws Exception
     */
    public boolean updateRelevanceFile(EvaluationTopic topic, String relevanceFileName) throws Exception
    {
        Gson gson = (new GsonBuilder()).serializeNulls().setPrettyPrinting().create();
        byte[] content = Files.readAllBytes(Paths.get(relevanceFileName));
        String fileContent = new String(content);
        boolean isUpdated = false;
        EvaluationTopic jsonEvaluationTopic = gson.fromJson(fileContent, EvaluationTopic.class);
        HashMap<Integer,HashMap<Integer,Integer>> hashMap = this.getRelevanceDecisionHashMap(relevanceFileName);

        for (EvaluationRelevance tempRelevance : topic.getRelevances()) {
            int hotelId = tempRelevance.getHotelId();
            int reviewId = tempRelevance.getReviewId();
            if (!hashMap.containsKey(hotelId) || !hashMap.get(hotelId).containsKey(reviewId)) {
                // hotel review combi does not exist in relevance file
                jsonEvaluationTopic.addRelevance(new EvaluationRelevance(hotelId, reviewId));
                isUpdated = true;
            }
        }

        if (isUpdated) {
            String updatedEvaluationTopic = gson.toJson(jsonEvaluationTopic, EvaluationTopic.class);
            PrintWriter writer = new PrintWriter(relevanceFileName, "UTF-8");
            writer.print(updatedEvaluationTopic);
            writer.close();
            logger.info("Relevance file [" + relevanceFileName + "] updated.");
        }

        return isUpdated;
    }

    /**
     * Get the average precision@k for a topic
     *
     * @param topic the topic to calculate the precision
     * @param relevanceFileName the file name of the relevance file
     * @return the precision value as double
     * @throws Exception
     */
    public double getAveragePrecisionOfTopic(EvaluationTopic topic, String relevanceFileName) throws Exception
    {
        double relevantCount = 0;
        double irrelevantCount = 0;
        double summarizedPrecisions = 0;
        HashMap<Integer,HashMap<Integer,Integer>> hashMap = this.getRelevanceDecisionHashMap(relevanceFileName);

        for (EvaluationRelevance tempRelevance : topic.getRelevances()) {
            int hotelId = tempRelevance.getHotelId();
            int reviewId = tempRelevance.getReviewId();
            if (hashMap.containsKey(hotelId) && hashMap.get(hotelId).containsKey(reviewId)) {
                if (hashMap.get(hotelId).get(reviewId) != -1) {
                    int relevance = hashMap.get(hotelId).get(reviewId);
                    if (relevance == 1) {
                        relevantCount++;
                        double precisionAtK = relevantCount / (relevantCount + irrelevantCount);
                        summarizedPrecisions += precisionAtK;
                    } else {
                        irrelevantCount++;
                    }
                } else {
                    // hotel review combi exists in relevance file but there is not relevance feedback given (still -1)
                    throw new FeedbackMissingException("No user relevance decision was done for topic [" + topic.getId() +
                            "] and hotelId [" + hotelId + "] - reviewId [" + reviewId +
                            "]. Please give a relevance feedback and run again.");
                }
            } else {
                // hotel review combi doesn't exist in relevance file, should be detected prior in updateRelevanceFile
                throw new RuntimeException("No user relevance decision was done for topic [" + topic.getId() +
                        "] and hotelId [" + hotelId + "] - reviewId [" + reviewId + "].");
            }
        }

        // Calculate the average precision for this topic - we divide by the number of relevant documents, if it is > 0
        double averagePrecisionOfTopic = 0;
        if (this._feedbackRelevanceCount > 0) {
            averagePrecisionOfTopic = summarizedPrecisions / this._feedbackRelevanceCount;
        }

        return averagePrecisionOfTopic;
    }

    /**
     * Get the average recall@k for a topic
     *
     * @param topic the topic to calculate the precision
     * @param relevanceFileName the file name of the relevance file
     * @return the recall value as double
     * @throws Exception
     */
    public double getAverageRecallOfTopic(EvaluationTopic topic, String relevanceFileName) throws Exception
    {
        double relevantCount = 0;
        double summarizedRecalls = 0;
        HashMap<Integer,HashMap<Integer,Integer>> hashMap = this.getRelevanceDecisionHashMap(relevanceFileName);

        for (EvaluationRelevance tempRelevance : topic.getRelevances()) {
            int hotelId = tempRelevance.getHotelId();
            int reviewId = tempRelevance.getReviewId();
            if (hashMap.containsKey(hotelId) && hashMap.get(hotelId).containsKey(reviewId)) {
                if (hashMap.get(hotelId).get(reviewId) != -1) {
                    int relevance = hashMap.get(hotelId).get(reviewId);
                    if (relevance == 1) {
                        relevantCount++;
                        double recallAtK = relevantCount / (this._feedbackRelevanceCount);
                        summarizedRecalls += recallAtK;
                    }
                } else {
                    // hotel review combi exists in relevance file but there is not relevance feedback given (still -1)
                    throw new FeedbackMissingException("No user relevance decision was done for topic [" + topic.getId() +
                            "] and hotelId [" + hotelId + "] - reviewId [" + reviewId +
                            "]. Please give a relevance feedback and run again.");
                }
            } else {
                // hotel review combi doesn't exist in relevance file, should be detected prior in updateRelevanceFile
                throw new RuntimeException("No user relevance decision was done for topic [" + topic.getId() +
                        "] and hotelId [" + hotelId + "] - reviewId [" + reviewId + "].");
            }
        }

        // Calculate the average recall for this topic - we divide by the number of relevant documents, if it is > 0
        double averageRecallOfTopic = 0;
        if (this._feedbackRelevanceCount > 0) {
            averageRecallOfTopic = summarizedRecalls / this._feedbackRelevanceCount;
        }

        return averageRecallOfTopic;
    }

    /**
     * Private function to fill a hashMap with all relevance decisions for quick access
     *
     * @param relevanceFileName file name of the relevance file
     * @return the hashMap
     * @throws IOException
     */
    private HashMap<Integer,HashMap<Integer,Integer>> getRelevanceDecisionHashMap(String relevanceFileName) throws IOException
    {
        this._feedbackRelevanceCount = 0;
        Gson gson = (new GsonBuilder()).serializeNulls().setPrettyPrinting().create();
        byte[] content = Files.readAllBytes(Paths.get(relevanceFileName));
        String fileContent = new String(content);

        EvaluationTopic jsonEvaluationTopic = gson.fromJson(fileContent, EvaluationTopic.class);

        // fill hashmap to quick access of relevance decisions from json file
        HashMap<Integer,HashMap<Integer,Integer>> hashMap = new HashMap<>();

        for (EvaluationRelevance tempRelevance : jsonEvaluationTopic.getRelevances()) {
            // Order doesn't play a role
            int hotelId = tempRelevance.getHotelId();
            int reviewId = tempRelevance.getReviewId();
            int relevance = tempRelevance.getIsRelevant();

            if (!hashMap.containsKey(hotelId)) {
                hashMap.put(hotelId,new HashMap<>());
            }
            if (relevance == 1) {
                // count the relevant documents for recall calculation
                this._feedbackRelevanceCount++;
            }
            hashMap.get(hotelId).put(reviewId, relevance);
        }

        return hashMap;
    }
}
