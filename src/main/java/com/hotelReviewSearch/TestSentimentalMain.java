package com.hotelReviewSearch;


import edu.stanford.nlp.simple.*;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;
import org.ejml.simple.SimpleMatrix;

import java.util.Properties;

public class TestSentimentalMain {

    /**
     * Quelle: https://stanfordnlp.github.io/CoreNLP/simple.html#usage
     * Public main method to trigger the sentimental analysis testSentimentPercentage
     * @param args arguments
     */
    public static void main(String[] args)
    {
        String text = "I have chosen this motel because of the good reviews. My expectations were high and I was not disappointed. Furthermore, I was impressed by the quality of the service and the room. We have been welcomed by the owner who was particularly helpful, caring and friendly. The room was clean and cozy. The breakfast served at room without extra charge was a great start of the day. There are plenty of nice walks and falls to see from there. It is walking distance from the train station. I dont normally write reviews, but everything was so great, I thought the least I can do is share my enthusiasm for the 3 Explorers Motel!";

        testSentimentSimple(text);
        testSentimentPercentage(text);
    }

    public static void testSentimentSimple(String text){

        System.out.println("Simple Test");
        System.out.println("##########################################");

        Document doc = new Document(text);
        for (Sentence sent : doc.sentences()) {
            System.out.println("------------------------------");
            System.out.println(sent);
            SentimentClass sentiment = sent.sentiment();
            System.out.println(sentiment.toString());
        }
    }

    /**
     * Quelle: https://www.programcreek.com/java-api-examples/index.php?source_dir=dkpro-core-master/de.tudarmstadt.ukp.dkpro.core.stanfordnlp-gpl/src/main/java/de/tudarmstadt/ukp/dkpro/core/stanfordnlp/StanfordSentimentAnalyzer.java
     */
    public static void testSentimentPercentage(String text) {

        System.out.println("Percentage Test");
        System.out.println("##########################################");

        StanfordCoreNLP pipeline;
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, pos, parse, sentiment");
        pipeline = new StanfordCoreNLP(props);


        Document doc = new Document(text);
        for (Sentence sent : doc.sentences()) {

            String sentenceText = sent.text();

            Annotation annotation = pipeline.process(sentenceText);

            for (CoreMap sentence : annotation.get(CoreAnnotations.SentencesAnnotation.class)) {
                Tree tree = sentence.get(SentimentCoreAnnotations.SentimentAnnotatedTree.class);
                SimpleMatrix sentimentCoefficients = RNNCoreAnnotations.getPredictions(tree);

                double veryNegative = sentimentCoefficients.get(0);
                double negative = sentimentCoefficients.get(1);
                double neutral = sentimentCoefficients.get(2);
                double positive = sentimentCoefficients.get(3);
                double veryPositive = sentimentCoefficients.get(4);

                System.out.println("------------------------");
                System.out.println(sentenceText);
                System.out.println("-- : " + veryNegative);
                System.out.println("- : " + negative);
                System.out.println("0 : " + neutral);
                System.out.println("+ : " + positive);
                System.out.println("++ : " + veryPositive);

            }
        }
    }



}
