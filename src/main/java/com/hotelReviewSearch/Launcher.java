package com.hotelReviewSearch;

import org.apache.lucene.queryparser.classic.ParseException;

import java.io.IOException;

/**
 * Main entry class.
 */
public class Launcher {

    /**
     * Main entry point to start either the web-portal or the aggregator.
     * @param args commandline arguments; either 'portal' or 'aggregator'
     */
    public static void main(String[] args) throws IOException, ParseException {
        String help = "Use the parameter 'webapp' to run the web application\n" +
                "Use 'index' to execute the indexing process.\n" +
                "Use 'eval' to create relevance judgments.";

        if (args.length == 1) {
            switch (args[0]){
                case "webapp":
                    Main.main(args);
                    break;

                case "index":
                    IndexMain.main(args);
                    break;

                case "eval":
                    EvaluationMain.main(args);
                    break;

                default:
                    System.out.println(help);
            }
        } else {
            System.out.println(help);
        }
    }
}
