package com.hotelReviewSearch.Entity;

import com.google.gson.annotations.SerializedName;

public class Variance {

    private int poor;
    private int average;
    @SerializedName("very good")
    private int veryGood;
    private int terrible;
    private int excellent;

    public Variance() {
    }

    public Variance(int poor, int average, int veryGood, int terrible, int excellent) {
        this.poor = poor;
        this.average = average;
        this.veryGood = veryGood;
        this.terrible = terrible;
        this.excellent = excellent;
    }

    public int getPoor() {
        return poor;
    }

    public void setPoor(int poor) {
        this.poor = poor;
    }

    public int getAverage() {
        return average;
    }

    public void setAverage(int average) {
        this.average = average;
    }

    public int getVeryGood() {
        return veryGood;
    }

    public void setVeryGood(int veryGood) {
        this.veryGood = veryGood;
    }

    public int getTerrible() {
        return terrible;
    }

    public void setTerrible(int terrible) {
        this.terrible = terrible;
    }

    public int getExcellent() {
        return excellent;
    }

    public void setExcellent(int excellent) {
        this.excellent = excellent;
    }
}
