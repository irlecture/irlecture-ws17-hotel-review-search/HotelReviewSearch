package com.hotelReviewSearch.Entity;

import com.hotelReviewSearch.Config.ConfigInterface;

public class Location implements ConfigInterface {

    public static String[] locationFields = {HOTEL_CITY, HOTEL_REGION, HOTEL_PROVINCE, HOTEL_COUNTRY, HOTEL_CONTINENT};
    private String country;
    private String continent;
    private String city;
    private String proposed_city;
    private String province;
    private String region;


    public Location() {
    }

    public Location(String country, String continent, String city, String proposed_city, String province, String region) {
        this.country = country;
        this.continent = continent;
        this.city = city;
        this.proposed_city = proposed_city;
        this.province = province;
        this.region = region;
    }

    public String getValueOfField(String fieldName){
        String result = "";

        switch (fieldName){
            case HOTEL_COUNTRY:
                result = getCountry();
                break;
            case HOTEL_CONTINENT:
                result = getContinent();
                break;
            case HOTEL_CITY:
                result = getCity();
                break;
            case HOTEL_PROVINCE:
                result = getProvince();
                break;
            case HOTEL_REGION:
                result = getRegion();
                break;
        }

        return result;
    }

    public void setValueOfField(String fieldName, String value){
        switch (fieldName){
            case HOTEL_COUNTRY:
                setCountry(value);
                break;
            case HOTEL_CONTINENT:
                setContinent(value);
                break;
            case HOTEL_CITY:
                setCity(value);
                break;
            case HOTEL_PROVINCE:
                setProvince(value);
                break;
            case HOTEL_REGION:
                setRegion(value);
                break;
        }
    }

    public String toString(){

        String result = "";

        if (city != null){
            result += "city: " + city;
        }
        if (proposed_city != null){
            result = addComma(result);
            result += "proposed_city: " + proposed_city;
        }
        if (province != null){
            result = addComma(result);
            result += "province: " + province;
        }
        if (region != null){
            result = addComma(result);
            result += "region: " + region;
        }
        if (country != null){
            result = addComma(result);
            result += "country: " + country;
        }
        if (continent != null){
            result = addComma(result);
            result += "continent: " + continent;
        }

        return result;
    }

    private String addComma(String string){
        if (string != null && !string.isEmpty()){
            string = string + ", ";
        }
        return string;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProposed_city() {
        return proposed_city;
    }

    public void setProposed_city(String proposed_city) {
        this.proposed_city = proposed_city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
