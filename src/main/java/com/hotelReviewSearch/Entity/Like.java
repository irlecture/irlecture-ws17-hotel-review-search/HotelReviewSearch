package com.hotelReviewSearch.Entity;

public class Like {

    private String disliked;
    private String liked;

    public Like() {
    }

    public Like(String disliked, String liked) {
        this.disliked = disliked;
        this.liked = liked;
    }

    public String getDisliked() {
        return disliked;
    }

    public void setDisliked(String disliked) {
        this.disliked = disliked;
    }

    public String getLiked() {
        return liked;
    }

    public void setLiked(String liked) {
        this.liked = liked;
    }
}
