package com.hotelReviewSearch.Entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Entity Class for the pagination of a generic List.
 * @param <T> Generic Type of the items in the page.
 */
public class PageOld<T> {

    public static final int DISPLAYED_PAGES_COUNT = 5;

    private int currentPage;
    private int pageSize;
    private int lastPage;
    private List<T> itemsOnPage;
    private boolean hasPreviousPage;
    private boolean hasNextPage;
    private int[] displayedPageNumbers;

    /**
     * Constructor to crate a Page from the given List.
     * @param allItems List of items to be paged.
     * @param currentPage Page to be displayed.
     * @param pageSize Number of items per page.
     */
    public PageOld(List<T> allItems, int currentPage, int pageSize) {

        int start, end, count, last;
        count = allItems.size();
        this.itemsOnPage = new ArrayList<>();

        if (count > 0) {
            // prüfe ob count durch pagesize teilbar ist
            if (count % pageSize == 0){
                // ist dies der Fall, wird die letzte Seite voll ausgefüllt
                last = count / pageSize;
            } else {
                // andernfalls ist eine zusätzliche Seite erforderlich
                // deshalb muss aufgerudnet werden
                last = (int) Math.ceil((double) count / (double) pageSize);
            }

            if (currentPage == 1){
                start = 0;
                this.hasPreviousPage = false;
            } else {
                start = (currentPage - 1) * pageSize;
                this.hasPreviousPage = true;
            }

            end = start + pageSize;
            // falls es nur eine Seite gibt
            if (end > count){
                end = count;
            }

            if (currentPage == last){
                this.hasNextPage = false;
            } else {
                this.hasNextPage = true;
            }

            this.itemsOnPage = allItems.subList(start, end);
            this.currentPage = currentPage;
            this.pageSize = pageSize;
            this.lastPage = last;
            setDisplayedPageNumbers(currentPage, last);
        }
    }

    /**
     * Set the numbers of the other pages around the current page, where the user can navigate to.
     * @param currentPage The current page (in most cases in center of the List)
     * @param lastPage The last page of the List.
     */
    private void setDisplayedPageNumbers(int currentPage, int lastPage){

        int[] displayedNumbers;

        // Anzahl der dargestellten Seiten zum Blättern festlegen
        if (lastPage < DISPLAYED_PAGES_COUNT){
            displayedNumbers = new int[lastPage];
        } else {
            displayedNumbers = new int[DISPLAYED_PAGES_COUNT];
        }

        int currentItem = currentPage - (DISPLAYED_PAGES_COUNT / 2);
        // die currentPage sollte in der Mitte der angezeigten Seiten sein
        for (int i=0; i < displayedNumbers.length; i++){

            displayedNumbers[i] = currentItem;
            currentItem++;
        }

        // für den Fall, dass die aktuelle eine der ersten Seiten ist => alles nach rechts verschieben
        while (displayedNumbers[0] < 1){

            for (int i=0; i < displayedNumbers.length; i++){
                displayedNumbers[i] += 1;
            }
        }

        // Für den Fall, dass das Seitenende erreicht ist
        while (true){
            if (displayedNumbers[displayedNumbers.length - 1] > lastPage){

                if (displayedNumbers[0] > 1){
                    // alle Seiten um eins nach links rutschen
                    for (int i=0; i < displayedNumbers.length; i++){
                        displayedNumbers[i] -= 1;
                    }

                } else {
                    // Seiten am Ende abschneiden, falls nicht genug Seiten vorhanden sind
                    int[] tempNumbers = new int[displayedNumbers.length - 1];
                    for (int i = 0; i < tempNumbers.length; i++){
                        tempNumbers[i] = displayedNumbers[i];
                    }
                    displayedNumbers = tempNumbers;
                }

            } else {
                break;
            }
        }

        this.displayedPageNumbers = displayedNumbers;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getLastPage() {
        return lastPage;
    }

    public void setLastPage(int lastPage) {
        this.lastPage = lastPage;
    }

    public List<T> getItemsOnPage() {
        return itemsOnPage;
    }

    public void setItemsOnPage(List<T> itemsOnPage) {
        this.itemsOnPage = itemsOnPage;
    }

    public boolean isHasPreviousPage() {
        return hasPreviousPage;
    }

    public void setHasPreviousPage(boolean hasPreviousPage) {
        this.hasPreviousPage = hasPreviousPage;
    }

    public boolean isHasNextPage() {
        return hasNextPage;
    }

    public void setHasNextPage(boolean hasNextPage) {
        this.hasNextPage = hasNextPage;
    }

    public int[] getDisplayedPageNumbers() {
        return displayedPageNumbers;
    }
}
