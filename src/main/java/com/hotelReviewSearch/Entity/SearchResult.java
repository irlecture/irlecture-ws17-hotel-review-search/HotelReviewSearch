package com.hotelReviewSearch.Entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Entity class for a single search result
 */
public class SearchResult {
    private Hotel hotel;
    private String hotelDescriptionSnippet;
    private int hitCountInHotel;
    private int maxCountReviews;
    public List<String> reviewSnippetList;

    public SearchResult(Hotel hotelEntity) {
        this.hotel = hotelEntity;

        if (hotel.getHotelDescription().isEmpty()) {
            this.hotelDescriptionSnippet = "No hotel description available.";
        } else {
            // If description has more than 200 chars, cut it there
            this.hotelDescriptionSnippet =
                    hotel.getHotelDescription().length() >= 200 ?
                            hotel.getHotelDescription().substring(0, 200) + " [...]" :
                            hotel.getHotelDescription();
        }

        this.reviewSnippetList = new ArrayList<>();
    }

    /**
     * Add a single review snippet to the search result
     *
     * @param reviewSnippet the snippet string to add
     * @return this SearchResult
     */
    public SearchResult addReviewSnippet(String reviewSnippet) {
        this.reviewSnippetList.add(reviewSnippet);
        return this;
    }
    public void setMaxCountReviews(int numberOfReviews) {this.maxCountReviews = numberOfReviews;}

    public Integer getMaxCountReviews() {return maxCountReviews;}

    public Hotel getHotel() {
        return hotel;
    }

    public List<String> getReviewSnippetList() {
        return reviewSnippetList;
    }

    public void setReviewSnippetList(List<String> reviewSnippetList) {
        this.reviewSnippetList = reviewSnippetList;
    }

    public String getHotelName() {
        return this.hotel.getHotelName();
    }

    public String getHotelDescriptionSnippet() {
        return hotelDescriptionSnippet;
    }

    public int getHitCountInHotel() {
        return hitCountInHotel;
    }

    public void setHitCountInHotel(int hitCountInHotel) {
        this.hitCountInHotel = hitCountInHotel;
    }

    public int getReviewSnippetCount() {
        return reviewSnippetList.size();
    }
}
