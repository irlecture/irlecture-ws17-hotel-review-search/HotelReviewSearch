package com.hotelReviewSearch.Entity;

import com.google.gson.annotations.SerializedName;
import org.apache.lucene.search.ScoreDoc;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Hotel entity class
 */
public class Hotel {

    private int hotelId;
    private String aka;
    @SerializedName("description")
    private String hotelDescription;
    private DetailRating detail_rating;
    private Location location;
    @SerializedName("name")
    private String hotelName;
    @SerializedName("places")
    private Place place;
    private int review_count;
    private int review_count_current;
    private List<ReviewEntity> reviews;
    private double slim_ranking;
    private float stars;
    @SerializedName("tripadvisor")
    private TripAdvisor tripAdvisor;
    private double user_rating;
    private Variance variance;
    private String jsonFile;
    /**
     * Necessary for searchAfter-Pagination.
     */
    private ScoreDoc scoreDoc;

    /**
     * Constructor for hotel entity
     *
     * @param hotelName the hotelName of the hotel
     * @param hotelDescription the hotel hotelDescription string
     */
    public Hotel(String hotelName, String hotelDescription) {
        if (hotelName == null) {
            hotelName = "Unknown hotel hotelName";
        }
        this.hotelName = hotelName;
        if (hotelDescription == null) {
            hotelDescription = "No hotel hotelDescription available";
        }
        this.hotelDescription = hotelDescription;
        this.reviews = new ArrayList<>();
    }

    public Hotel() {
    }

    public void setReviews(List<ReviewEntity> reviews) {
        this.reviews = reviews;
    }

    public List<ReviewEntity> getReviews() {
        return reviews;
    }

    public Hotel addReview(ReviewEntity review) {
        this.reviews.add(review);
        return this;
    }

    public int getHotelId() {
        return hotelId;
    }

    public void setHotelId(int hotelId) {
        this.hotelId = hotelId;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Variance getVariance() {
        return variance;
    }

    public void setVariance(Variance variance) {
        this.variance = variance;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getHotelDescription() {
        return hotelDescription;
    }

    public void setHotelDescription(String hotelDescription) {
        this.hotelDescription = hotelDescription;
    }

    public double getUser_rating() {
        return user_rating;
    }

    public void setUser_rating(double user_rating) {
        this.user_rating = user_rating;
    }

    public int getReview_count() {
        return review_count;
    }

    public void setReview_count(int review_count) {
        this.review_count = review_count;
    }

    public String getAka() {
        return aka;
    }

    public void setAka(String aka) {
        this.aka = aka;
    }

    public double getSlim_ranking() {
        return slim_ranking;
    }

    public void setSlim_ranking(double slim_ranking) {
        this.slim_ranking = slim_ranking;
    }

    public DetailRating getDetail_rating() {
        return detail_rating;
    }

    public void setDetail_rating(DetailRating detail_rating) {
        this.detail_rating = detail_rating;
    }

    public String getJsonFile() {
        return jsonFile;
    }

    public void setJsonFile(String jsonFile) {
        this.jsonFile = jsonFile;
    }

    public ScoreDoc getScoreDoc() {
        return scoreDoc;
    }

    public void setScoreDoc(ScoreDoc scoreDoc) {
        this.scoreDoc = scoreDoc;
    }
}
