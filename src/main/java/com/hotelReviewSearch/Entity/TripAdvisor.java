package com.hotelReviewSearch.Entity;

import com.google.gson.annotations.SerializedName;

public class TripAdvisor {

    @SerializedName("VISITED")
    private boolean visited;
    private String locality;
    private String country_name;
    private String street_address;
    private String pic_link;

    public TripAdvisor() {
    }

    public TripAdvisor(boolean visited, String locality, String country_name, String street_address, String pic_link) {
        this.visited = visited;
        this.locality = locality;
        this.country_name = country_name;
        this.street_address = street_address;
        this.pic_link = pic_link;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getStreet_address() {
        return street_address;
    }

    public void setStreet_address(String street_address) {
        this.street_address = street_address;
    }

    public String getPic_link() {
        return pic_link;
    }

    public void setPic_link(String pic_link) {
        this.pic_link = pic_link;
    }
}
