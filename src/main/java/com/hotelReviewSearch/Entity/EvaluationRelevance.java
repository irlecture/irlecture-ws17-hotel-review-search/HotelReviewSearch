package com.hotelReviewSearch.Entity;

/**
 * Entity class to define a relevance decision for a topic
 */
public class EvaluationRelevance {
    private int hotelId;
    private int reviewId;
    /**
     * -1 : no feedback given
     * 0  : not relevant
     * 1  : is relevant
     */
    private int isRelevant;

    public EvaluationRelevance(int hotelId, int reviewId) {
        this.hotelId = hotelId;
        this.reviewId = reviewId;
        this.isRelevant = -1;
    }

    public int getHotelId() {
        return hotelId;
    }

    public void setHotelId(int hotelId) {
        this.hotelId = hotelId;
    }

    public int getReviewId() {
        return reviewId;
    }

    public void setReviewId(int reviewId) {
        this.reviewId = reviewId;
    }

    public int getIsRelevant() {
        return isRelevant;
    }

    public void setIsRelevant(int isRelevant) {
        this.isRelevant = isRelevant;
    }
}
