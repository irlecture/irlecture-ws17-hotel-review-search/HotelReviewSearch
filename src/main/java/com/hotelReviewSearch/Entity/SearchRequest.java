package com.hotelReviewSearch.Entity;

import java.util.Date;

/**
 * Class to save metadata for a search request
 */
public class SearchRequest
{
    private String searchString;
    private int pageNumber;
    private int itemsPerPage;
    private String remoteAddress;
    private String requestUrl;
    private Date requestTime;
    private Date responseTime;

    /**
     * Overload constructor
     *
     * @param searchString the query string to search for
     * @param pageNumber the page number
     * @param itemsPerPage the number of items to display
     */
    public SearchRequest(String searchString, int pageNumber, int itemsPerPage) {
        this.searchString = searchString;
        this.pageNumber = pageNumber;
        this.itemsPerPage = itemsPerPage;
        this.requestTime = new Date();
    }

    public SearchRequest(String searchString) {
        this.searchString = searchString;
    }

    public SearchRequest() {
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(int itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public SearchRequest setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
        return this;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public SearchRequest setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
        return this;
    }

    public Date getRequestTime() {
        return requestTime;
    }

    public SearchRequest setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
        return this;
    }

    public Date getResponseTime() {
        return responseTime;
    }

    public SearchRequest setResponseTime(Date responseTime) {
        this.responseTime = responseTime;
        return this;
    }
}
