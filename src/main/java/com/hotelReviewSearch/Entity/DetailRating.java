package com.hotelReviewSearch.Entity;

import com.sun.javafx.collections.MappingChange;

import java.util.HashMap;
import java.util.Map;

/**
 * Entity Class for Detail Ranking of a Hotel based on the data of the tripadvisor data set.
 */
public class DetailRating {

    private float business;
    private float cleanliness;
    private float desk;
    private float location;
    private float pool;
    private float restaurant;
    private float rooms;
    private float service;
    private float sleep;
    private float value;

    public DetailRating() {
    }

    public DetailRating(float business, float cleanliness, float desk, float location, float pool, float restaurant, float rooms, float service, float sleep, float value) {
        this.business = business;
        this.cleanliness = cleanliness;
        this.desk = desk;
        this.location = location;
        this.pool = pool;
        this.restaurant = restaurant;
        this.rooms = rooms;
        this.service = service;
        this.sleep = sleep;
        this.value = value;
    }

    public float getBusiness() {
        return business;
    }

    public void setBusiness(float business) {
        this.business = business;
    }

    public float getCleanliness() {
        return cleanliness;
    }

    public void setCleanliness(float cleanliness) {
        this.cleanliness = cleanliness;
    }

    public float getDesk() {
        return desk;
    }

    public void setDesk(float desk) {
        this.desk = desk;
    }

    public float getLocation() {
        return location;
    }

    public void setLocation(float location) {
        this.location = location;
    }

    public float getPool() {
        return pool;
    }

    public void setPool(float pool) {
        this.pool = pool;
    }

    public float getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(float restaurant) {
        this.restaurant = restaurant;
    }

    public float getRooms() {
        return rooms;
    }

    public void setRooms(float rooms) {
        this.rooms = rooms;
    }

    public float getService() {
        return service;
    }

    public void setService(float service) {
        this.service = service;
    }

    public float getSleep() {
        return sleep;
    }

    public void setSleep(float sleep) {
        this.sleep = sleep;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public Map<String, Float> getDetailRatingMap(){
        Map<String, Float> detailRatingMap = new HashMap<>();

        if (this.getBusiness() > 0.0) detailRatingMap.put("business", this.getBusiness());
        if (this.getCleanliness() > 0.0) detailRatingMap.put("cleanliness", this.getCleanliness());
        if (this.getDesk() > 0.0) detailRatingMap.put("desk", this.getDesk());
        if (this.getLocation() > 0.0) detailRatingMap.put("location", this.getLocation());
        if (this.getPool() > 0.0) detailRatingMap.put("pool", this.getPool());
        if (this.getRestaurant() > 0.0) detailRatingMap.put("restaurant", this.getRestaurant());
        if (this.getRooms() > 0.0) detailRatingMap.put("rooms", this.getRooms());
        if (this.getService() > 0.0) detailRatingMap.put("service", this.getService());
        if (this.getSleep() > 0.0) detailRatingMap.put("sleep", this.getSleep());
        if (this.getValue() > 0.0) detailRatingMap.put("value", this.getValue());

        return detailRatingMap;
    }
}
