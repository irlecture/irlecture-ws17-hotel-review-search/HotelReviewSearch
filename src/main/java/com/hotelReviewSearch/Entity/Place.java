package com.hotelReviewSearch.Entity;

import com.google.gson.JsonArray;

import java.util.List;

public class Place {

    private JsonArray aka;
    private JsonArray hotel_description;
    private JsonArray hotel_name;

//    private JsonArray aka;
//    private JsonArray hotel_description;
//    private JsonArray hotel_name;

    public Place(){

    }

    public Place(JsonArray aka, JsonArray hotel_description, JsonArray hotel_name) {
        this.aka = aka;
        this.hotel_description = hotel_description;
        this.hotel_name = hotel_name;
    }

    public JsonArray getAka() {
        return aka;
    }

    public void setAka(JsonArray aka) {
        this.aka = aka;
    }

    public JsonArray getHotel_description() {
        return hotel_description;
    }

    public void setHotel_description(JsonArray hotel_description) {
        this.hotel_description = hotel_description;
    }

    public JsonArray getHotel_name() {
        return hotel_name;
    }

    public void setHotel_name(JsonArray hotel_name) {
        this.hotel_name = hotel_name;
    }
}
