package com.hotelReviewSearch.Entity;

/**
 * Entity class for a hotel review
 */
public class ReviewEntity
{
    private String date;
    private DetailRating detail_rating;
    private Integer helpfulness;
    private int id;
    private Like likes;
    private float rating;
    private String stay_parameters;
    private String text;
    private String title;
    private String user;
    private boolean isMatch = false;

    public ReviewEntity() {
    }

    public ReviewEntity(String title, String text) {
        this.title = title;
        this.text = text;
    }

    public ReviewEntity(String date, DetailRating detail_rating, Integer helpfulness, int id, Like likes, float rating, String stay_parameters, String text, String title, String user) {
        this.date = date;
        this.detail_rating = detail_rating;
        this.helpfulness = helpfulness;
        this.id = id;
        this.likes = likes;
        this.rating = rating;
        this.stay_parameters = stay_parameters;
        this.text = text;
        this.title = title;
        this.user = user;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public DetailRating getDetail_rating() {
        return detail_rating;
    }

    public void setDetail_rating(DetailRating detail_rating) {
        this.detail_rating = detail_rating;
    }

    public Integer getHelpfulness() {
        return helpfulness;
    }

    public void setHelpfulness(Integer helpfulness) {
        this.helpfulness = helpfulness;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Like getLikes() {
        return likes;
    }

    public void setLikes(Like likes) {
        this.likes = likes;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getStay_parameters() {
        return stay_parameters;
    }

    public void setStay_parameters(String stay_parameters) {
        this.stay_parameters = stay_parameters;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public boolean isMatch() {
        return isMatch;
    }

    public ReviewEntity setMatch(boolean match) {
        isMatch = match;
        return this;
    }
}
