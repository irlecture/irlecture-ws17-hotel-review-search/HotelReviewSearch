package com.hotelReviewSearch.Entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Entity class to define a topic of the evaluation system
 */
public class EvaluationTopic
{
    private String id;
    private String title;
    private String description;
    private String narrative;
    private List<EvaluationRelevance> relevances;

    public EvaluationTopic(String id, String title, String description, String narrative) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.narrative = narrative;
        this.relevances = new ArrayList<>();
    }

    public EvaluationTopic() {
        this.relevances = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNarrative() {
        return narrative;
    }

    public void setNarrative(String narrative) {
        this.narrative = narrative;
    }

    public List<EvaluationRelevance> getRelevances() {
        return relevances;
    }

    public void setRelevances(List<EvaluationRelevance> relevances) {
        this.relevances = relevances;
    }

    public EvaluationTopic addRelevance(EvaluationRelevance evaluationRelevance) {
        this.relevances.add(evaluationRelevance);
        return this;
    }
}
