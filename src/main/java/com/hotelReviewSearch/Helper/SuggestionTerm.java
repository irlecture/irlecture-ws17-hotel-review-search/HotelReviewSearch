package com.hotelReviewSearch.Helper;

public class SuggestionTerm {

    private String value;
    private long weight;

    public SuggestionTerm(String value, long weight) {
        this.value = value;
        this.weight = weight;
    }

    public long getWeight() {
        return weight;
    }

    public void setWeight(long weight) {
        this.weight = weight;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
