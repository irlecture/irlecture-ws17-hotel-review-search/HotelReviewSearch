package com.hotelReviewSearch.Helper;

import com.hotelReviewSearch.Config.ConfigInterface;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.standard.StandardAnalyzer;

/**
 * Helper-Class to initialize global StopWord List with the existing StopWord List from the StandardAnalyzer
 * and the StopWord List from the ConfigInterface.
 */
public class StopWordHelper implements ConfigInterface{

    private static CharArraySet hotelReviewStopWords;
    private static CharArraySet locationStopWords;

    static {
        StandardAnalyzer standardAnalyzer = new StandardAnalyzer();
        hotelReviewStopWords = new CharArraySet(standardAnalyzer.getStopwordSet(), true);
        locationStopWords = new CharArraySet(standardAnalyzer.getStopwordSet(), true);

        locationStopWords.addAll(customLocationSearchStopWords);
        if (customStopWordListActivated){
            hotelReviewStopWords.addAll(customHotelReviewSearchStopWords);
        }
    }

    /**
     * Get a Set of StopWords for a StandardAnalyzer to be used in a search for hotel reviews.
     * @return Global StopWordSet for hotel reviews.
     */
    public static CharArraySet getHotelReviewStopWords(){
        return hotelReviewStopWords;
    }

    /**
     * Get a Set of StopWords for a StandardAnalyzer to be used in a search for locations.
     * @return Global StopWordSet for locations
     */
    public static CharArraySet getLocationStopWords() {
        return locationStopWords;
    }
}
