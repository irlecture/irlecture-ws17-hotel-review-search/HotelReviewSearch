package com.hotelReviewSearch.Helper;

import org.apache.lucene.search.suggest.InputIterator;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

public class SuggestionIterator implements InputIterator {

    private Iterator<SuggestionTerm> innerIterator;
    private SuggestionTerm currentTerm;

    public SuggestionIterator(Iterator<SuggestionTerm> innerIterator) {
        this.innerIterator = innerIterator;
    }

    @Override
    public long weight() {
        return currentTerm.getWeight();
    }

    @Override
    public BytesRef payload() {
        return null;
    }

    @Override
    public boolean hasPayloads() {
        return false;
    }

    @Override
    public Set<BytesRef> contexts() {
        // toDO: hier könnte man die schon vorhandenen Suchterme reinpacken
        return null;
    }

    @Override
    public boolean hasContexts() {
        return false;
    }

    @Override
    public BytesRef next() throws IOException {
        if (innerIterator.hasNext()) {
            currentTerm = innerIterator.next();
            return new BytesRef(currentTerm.getValue().getBytes("UTF8"));
        } else {
            return null;
        }
    }
}
