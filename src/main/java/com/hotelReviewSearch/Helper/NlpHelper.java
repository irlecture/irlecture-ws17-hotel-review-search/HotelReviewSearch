package com.hotelReviewSearch.Helper;

import com.hotelReviewSearch.Config.ConfigInterface;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class NlpHelper
{
    /**
     * Converts the String into an array of nGrams.
     * Source: https://stackoverflow.com/questions/3656762/n-gram-generation-from-a-sentence
     * @param n
     * @param text
     * @return
     */
    public static Collection<String> getNGrams(int n, String text, Collection<String> stopWords) {

        Collection<String> results = new ArrayList<>();

        for (int i = 1; i <= n; i++) {
            for (String ngram : ngrams(i, text)){
                results.add(ngram);
            }
        }

        return removeNGramStopWords(results, stopWords);
    }

    private static Collection<String> removeNGramStopWords(Collection<String> targetNGrams, Collection<String> stopWords){
        Collection<String> resultNGrams = new ArrayList<>(targetNGrams);
        //remove queryNGram Stopwords
        for (String filterNgram : stopWords){
            if (resultNGrams.contains(filterNgram)){
                resultNGrams.remove(filterNgram);
            }
        }
        return resultNGrams;
    }

    private static List<String> ngrams(int n, String str) {
        List<String> ngrams = new ArrayList<>();
        String[] words = str.split("\\s");
        for (int i = 0; i < words.length - n + 1; i++)
            ngrams.add(concat(words, i, i+n));
        return ngrams;
    }

    private static String concat(String[] words, int start, int end) {
        StringBuilder sb = new StringBuilder();
        for (int i = start; i < end; i++)
            sb.append((i > start ? " " : "") + words[i]);
        return sb.toString();
    }

}
