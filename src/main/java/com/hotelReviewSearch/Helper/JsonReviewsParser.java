package com.hotelReviewSearch.Helper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

/**
 * Helper class for json parsing logic
 */
public class JsonReviewsParser {

    private static final Logger logger = LogManager.getLogger(JsonReviewsParser.class);

    public static JSONArray parseJsonArray(String jsonFilePath) throws IOException {
        // Parses and returns a JSON array (Array with nested JSON objects)

        JSONParser jsonParser = new JSONParser();
        JSONArray jsonArray = null;

        try{
            Object object = jsonParser.parse(new FileReader(jsonFilePath));
            jsonArray = (JSONArray) object;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    /**
     * Parses a json object from a given file and returns it
     *
     * @param jsonFilePath the path to the json file
     * @return a json object or null, if a exception was thrown
     */
    public static JSONObject parseJsonObject(String jsonFilePath) {
        // Parses a JSON object and returns it.
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = null;

        try {
            Object object = jsonParser.parse(new FileReader(jsonFilePath));
            jsonObject = (JSONObject) object;
        } catch (ParseException | IOException e) {
            logger.error("Error while parsing file [" + jsonFilePath + "].");
        }
        return jsonObject;
    }
}
