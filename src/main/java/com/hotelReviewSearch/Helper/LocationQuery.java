package com.hotelReviewSearch.Helper;

import com.hotelReviewSearch.Entity.Location;

public class LocationQuery {

    private String originalQueryString;
    private String editedQueryString;
    private Location location;

    public LocationQuery() {
    }

    public LocationQuery(String originalQueryString, Location location) {
        this.originalQueryString = originalQueryString;
        this.location = location;
    }

    public String getOriginalQueryString() {
        return originalQueryString;
    }

    public void setOriginalQueryString(String originalQueryString) {
        this.originalQueryString = originalQueryString;
    }

    public String getEditedQueryString() {
        return editedQueryString;
    }

    public void setEditedQueryString(String editedQueryString) {
        this.editedQueryString = editedQueryString;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
