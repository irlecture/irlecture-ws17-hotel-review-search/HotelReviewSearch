package com.hotelReviewSearch.Helper;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.FileVisitResult.CONTINUE;

public class DirectoryWalker  extends SimpleFileVisitor<Path> {
    @Override
    public FileVisitResult visitFile(Path path, BasicFileAttributes basicFileAttributes) {
        if (basicFileAttributes.isRegularFile()) {
            String extension = FilenameUtils.getExtension(new File(String.valueOf(path)).getName());
            if (extension.equals("json")) {
                System.out.println("path = " + path);
            }
//            System.out.println(path + " is a regular file with size "
//                    + basicFileAttributes.size());
        } else if (basicFileAttributes.isSymbolicLink()) {
            System.out.println(path + " is a symbolic link.");
        } else {
            System.out.println(path
                    + " is not a regular file or symbolic link.");
        }
        return CONTINUE;
    }

//    @Override
//    public FileVisitResult postVisitDirectory(Path path, IOException exc) {
//        System.out.println(path + " visited.");
//        return CONTINUE;
//    }
//
//    @Override
//    public FileVisitResult visitFileFailed(Path file, IOException ioException) {
//        System.err.println(ioException);
//        return CONTINUE;
//    }

    public ArrayList<Path> walkDirTree(Path root) throws IOException
    {
        ArrayList <Path> paths = new ArrayList<>();
        Files.walk(Paths.get(String.valueOf(root))).forEach(path ->
        {
            try
            {
                BasicFileAttributes basicFileAttributes = Files.readAttributes(path, BasicFileAttributes.class);
                if (basicFileAttributes.isRegularFile())
                {
                    String extension = FilenameUtils.getExtension(new File(String.valueOf(path)).getName());
                    if (extension.equals("json"))
                    {
                        paths.add(path);
                    }
                }
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        });
        return paths;
    }
}
