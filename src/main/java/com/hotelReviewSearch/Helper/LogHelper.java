package com.hotelReviewSearch.Helper;

import com.hotelReviewSearch.Entity.SearchRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Helper class for logging logic
 */
public class LogHelper {

    private static final Logger requestLogger = LogManager.getLogger("RequestLogger");

    /**
     * Write a SearchRequest object to the logfile which is configured under name RequestLogger
     *
     * @param searchRequest the SearchRequest object to log
     */
    public static void writeRequestToLog(SearchRequest searchRequest) {
        Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if (searchRequest.getResponseTime() == null) {
            searchRequest.setResponseTime(new Date());
        }
        String logMessage = String.format(
                "IP: [%s], Request-Url: [%s] Query: [%s], Page: [%d], ItemsPerPage: [%d], RequestTime: %s, ResponseTime: %s, Duration [ms]: %d",
                searchRequest.getRemoteAddress(),
                searchRequest.getRequestUrl(),
                searchRequest.getSearchString(),
                searchRequest.getPageNumber(),
                searchRequest.getItemsPerPage(),
                formatter.format(searchRequest.getRequestTime()),
                formatter.format(searchRequest.getResponseTime()),
                searchRequest.getResponseTime().getTime() - searchRequest.getRequestTime().getTime()
        );
        requestLogger.info(logMessage);
    }
}
