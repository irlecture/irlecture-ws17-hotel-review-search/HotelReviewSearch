package com.hotelReviewSearch.Dao;

import com.google.gson.Gson;
import com.hotelReviewSearch.Config.ConfigInterface;
import com.hotelReviewSearch.Entity.Hotel;
import com.hotelReviewSearch.Helper.JsonReviewsParser;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Set;

@Repository
@Qualifier("fakeHotelData")
public class FakeHotelDao implements HotelDaoInterface, ConfigInterface{

    @Override
    public Hotel getHotelById(int hotelId) throws IOException {

        File firstHotelFile = new File(reviewPath).listFiles()[hotelId];

        byte[] content = Files.readAllBytes(firstHotelFile.toPath());
        String fileContent = new String(content);   // eventuell muss hier das encoding noch angepasst werden

        Hotel resultHotel = new Gson().fromJson(fileContent, Hotel.class);

        return resultHotel;
    }

    @Override
    public Hotel getHotelWithTopReviewsById(int hotelId, String queryString, int topCount) throws Exception {
        return this.getHotelById(hotelId);
    }
}
