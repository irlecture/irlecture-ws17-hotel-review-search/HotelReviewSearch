package com.hotelReviewSearch.Dao;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hotelReviewSearch.Config.ConfigInterface;
import com.hotelReviewSearch.Entity.*;
import com.hotelReviewSearch.Helper.LocationQuery;
import com.hotelReviewSearch.Helper.NlpHelper;
import com.hotelReviewSearch.Helper.StopWordHelper;
import me.xdrop.fuzzywuzzy.FuzzySearch;
import me.xdrop.fuzzywuzzy.model.ExtractedResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.IntPoint;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Fields;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.simple.SimpleQueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.search.highlight.*;
import org.apache.lucene.search.highlight.Formatter;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Repository
@Qualifier("liveHotelReviewData")
public class SearchEngineHotelReviewsDao implements SearchDaoInterface, ConfigInterface, HotelDaoInterface {

    private final Logger logger = LogManager.getLogger(SearchEngineHotelReviewsDao.class);

    private int totalHits = 0;

    /**
     * Get all matching Location-Fields in the Corpus for the given query.
     * @param queryString
     * @return A Location Object with the matching Locations.
     * @throws IOException
     * @throws ParseException
     */
    public LocationQuery getLocationsInQuery(String queryString) throws IOException, ParseException {

        Location resultLocation = new Location();
        String resultQueryString = queryString;
        LocationQuery resultLocationQuery = new LocationQuery(queryString, resultLocation);

        if (queryString.isEmpty()) {
            return resultLocationQuery;
        }

        StandardAnalyzer analyzer = new StandardAnalyzer(StopWordHelper.getLocationStopWords());

        File hotelReviewIndexDirectoryFile = new File(hotelReviewIndexDirectory);
        Directory hotelReviewIndexDirectory = FSDirectory.open(hotelReviewIndexDirectoryFile.toPath());
        IndexReader reader = DirectoryReader.open(hotelReviewIndexDirectory);
        IndexSearcher searcher = new IndexSearcher(reader);

        // 1) Create nGrams for the Query with a StopWord Filter => they will be looked up in 2)
        Collection<String> queryNGrams = NlpHelper.getNGrams(3, queryString, locationBackwardsSearchFilter);

        for (String field: Location.locationFields) {
            Query query = new SimpleQueryParser(analyzer, field).parse(queryString);
            TopDocs topDocs = searcher.search(query, 1);

            ScoreDoc[] hits = topDocs.scoreDocs;
            if (hits.length > 0){
                ScoreDoc firstHit = hits[0];
                Document d = searcher.doc(hits[0].doc);

                String searchResult = d.get(field);

                // 2) check if the query really contains the found location => the fuzzy match of the best nGram
                //    must equal the found location more than 90%
                // e.g. 'surf camp' would match the wrong location 'fish camp' => such mistakes get filtered out
                ExtractedResult extractedResult = FuzzySearch.extractOne(searchResult, queryNGrams);
                //System.out.println("\t#found in Query: " + extractedResult.getString() + " - " + extractedResult.getScore() + " - " + field + " - " + searchResult);

                if (extractedResult.getScore() > 90){

                    // unboost the extraced nGram in queryString
                    resultQueryString = unboostLocationTerms(queryString, extractedResult.getString());
                    resultLocation.setValueOfField(field, searchResult);
                }
            }
        }
        resultLocationQuery.setEditedQueryString(resultQueryString);
        return resultLocationQuery;
    }

    /**
     * Expands the extracted location terms in a query with their punishment factor.
     * e.g. 'nice hotel berlin dogs' => 'nice hotel berlin^0.1 dogs'
     * @param query original query by the user
     * @param unboostTerms extraced location terms to unboost
     * @return
     */
    private String unboostLocationTerms(String query, String unboostTerms){

        String result = query;
        String[] terms = unboostTerms.split(" ");

        for (String term: terms) {
            result = result.replace(term, term + "^" + locationTermPunishment);
        }

        return result;
    }

    /**
     * Get a hotel entity by its id from index and read the hotel data from json file path of the index
     * @param hotelId the hotel id to fetch
     * @return a hotel entity with data from json file
     * @throws IOException
     */
    @Override
    public Hotel getHotelById(int hotelId) throws IOException {
        File hotelIndexDirectoryFile = new File(hotelReviewIndexDirectory);
        Directory hotelIndexDirectory = FSDirectory.open(hotelIndexDirectoryFile.toPath());

        // Create a new exact query to search for this special hotelId
        Query exactQuery = IntPoint.newExactQuery(HOTEL_ID_POINT, hotelId);

        IndexReader reader = DirectoryReader.open(hotelIndexDirectory);
        IndexSearcher searcher = new IndexSearcher(reader);
        searcher.setSimilarity(retrievalModel);

        TopDocs topDocs = searcher.search(exactQuery, 1);
        if (topDocs.scoreDocs.length > 0){
            ScoreDoc hit = topDocs.scoreDocs[0];

            int docId = hit.doc;
            Document d = searcher.doc(docId);

            String jsonFile = d.get(JSON_FILE);
            Hotel hotel = parseHotelFromJsonFile(jsonFile);
            hotel.setJsonFile(d.get(JSON_FILE));
            hotel.setHotelId(Integer.parseInt(d.get(HOTEL_ID)));
            hotel.setScoreDoc(hit); // ToDo: nötig?

            return hotel;
        } else {
            return null;
        }
    }

    /**
     * Get a hotel entity by its id with a ranked list of reviews for the detail view
     *
     * @param hotelId the hotel id to fetch
     * @param queryString the query string to score the reviews
     * @param topCount the amount of reviews to fetch
     * @return a hotel entity with <b>topCount</b> ranked reviews and all other reviews from the json file
     * @throws Exception
     */
    @Override
    public Hotel getHotelWithTopReviewsById(int hotelId, String queryString, int topCount) throws Exception
    {
        // 1. Get hotel from index and json file
        Hotel hotel = this.getHotelById(hotelId);
        // 2. Get top reviews for hotel and query
        List<ReviewEntity> topReviewsOfHotel = this.getTopReviewsOfHotel(hotelId, queryString, topCount);
        // 3. Get reviews from json
        List<ReviewEntity> jsonReviews = hotel.getReviews();
        // 4. Merge reviews from json and search engine
        HashMap<Integer, ReviewEntity> reviewHashMap = new HashMap<>();
        List<ReviewEntity> finalReviewList = new ArrayList<>();
        HashSet<Integer> currentlyInsertedReviewIds = new HashSet<>();
        for (ReviewEntity jsonReview : jsonReviews) {
            reviewHashMap.put(jsonReview.getId(), jsonReview);
        }
        for (ReviewEntity topReview : topReviewsOfHotel) {
            // todo check if exists
            ReviewEntity tempReviewEntity = reviewHashMap.get(topReview.getId());
            tempReviewEntity
                    .setMatch(true)
                    // Set text from index because of the highlighting
                    .setText(topReview.getText());
            finalReviewList.add(tempReviewEntity);
            currentlyInsertedReviewIds.add(topReview.getId());
        }
        for (ReviewEntity jsonReview : jsonReviews) {
            if (!currentlyInsertedReviewIds.contains(jsonReview.getId())) {
                finalReviewList.add(jsonReview);
            }
        }

        // 5. double check that all reviews has to be inserted
        if (finalReviewList.size() != jsonReviews.size()) {
            throw new Exception("Not all reviews inserted.");
        }

        hotel.setReviews(finalReviewList);

        return hotel;
    }

    /**
     * Get a hotel entity with data from the given json file
     *
     * @param pathToJsonFile the path to the json file
     * @return the hotel entity
     * @throws IOException if an error occurs while parsing the json
     */
    private Hotel parseHotelFromJsonFile(String pathToJsonFile) throws IOException {
        byte[] content = Files.readAllBytes(Paths.get(pathToJsonFile));
        String fileContent = new String(content);   // eventuell muss hier das encoding noch angepasst werden
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        Gson gson = gsonBuilder.create();
        return gson.fromJson(fileContent, Hotel.class);
    }

    /**
     * Get the search results page for the search view
     *
     * @param queryString the query string to search for
     * @param page the page number
     * @param hitsPerPage the amount of items per page
     * @return a page entity with search results
     * @throws IOException
     * @throws ParseException
     * @throws InvalidTokenOffsetsException
     */
    @Override
    public Page<SearchResult> getSearchResultsPage(String queryString, int page, int hitsPerPage) throws IOException, ParseException, InvalidTokenOffsetsException {
        ArrayList<SearchResult> searchResults = new ArrayList<>();
        String queryStringUsed = queryString;

        if (queryStringUsed.isEmpty()) {
            return new Page<SearchResult>(searchResults, page, hitsPerPage, 0);
        }

        // 1) get locations in the query
        LocationQuery locationQuery = null;
        if (locationSearchActicated){
            locationQuery = getLocationsInQuery(queryStringUsed);
            queryStringUsed = locationQuery.getEditedQueryString();
        }


        // 2) get top hotels for the query
        List<Hotel> topHotels = this.getTopHotelsPaginated(queryStringUsed, page, hitsPerPage, locationQuery);

        for (Hotel hotel : topHotels) {
            SearchResult searchResult = new SearchResult(hotel);
            // 3) get Reviews in the right order for each Hotel
            searchResult.setReviewSnippetList(getTopReviewSnippetsOfHotel(hotel.getHotelId(), queryStringUsed));
            searchResult.setMaxCountReviews(getMaxReviewsProHotel(hotel));
            searchResult.setHitCountInHotel(getHitsInHotel(hotel.getHotelId(), queryStringUsed));
            searchResults.add(searchResult);

        }

        Page<SearchResult> resultPage = new Page<SearchResult>(searchResults, page, hitsPerPage, this.totalHits);

        return resultPage;
    }

    /**
     * Get a paginated list of ranked hotels
     *
     * @param queryString the query string to search for
     * @param page the number of the page
     * @param hitsPerPage the amount of items on the page
     * @return a list of hotel entities
     * @throws IOException
     * @throws ParseException
     */
    private List<Hotel> getTopHotelsPaginated(String queryString, int page, int hitsPerPage, LocationQuery locationQuery) throws IOException, ParseException {
        ArrayList<Hotel> topHotels = new ArrayList<>();

        if (queryString.isEmpty()) {
            return topHotels;
        }

        Query query = null;

        StandardAnalyzer analyzer = new StandardAnalyzer(StopWordHelper.getHotelReviewStopWords());

        File hotelReviewIndexDirectoryFile = new File(hotelReviewIndexDirectory);
        Directory hotelReviewIndexDirectory = FSDirectory.open(hotelReviewIndexDirectoryFile.toPath());

        // Fields to search on
        String[] fields = {HOTEL_NAME, HOTEL_DESCRIPTION, REVIEW_TITLE, REVIEW_TEXT};
        BooleanClause.Occur[] flags = {
                BooleanClause.Occur.SHOULD,
                BooleanClause.Occur.SHOULD,
                BooleanClause.Occur.SHOULD,
                BooleanClause.Occur.SHOULD
        };
        Query hotelAndReviewTextsQuery = MultiFieldQueryParser.parse(queryString, fields, flags, analyzer);

        if (locationSearchActicated){
            // A - seach on HOTEL and REVIEW fields + LOCATION fields
            BooleanQuery.Builder booleanQueryBuilder = new BooleanQuery.Builder();
            booleanQueryBuilder.add(hotelAndReviewTextsQuery, BooleanClause.Occur.MUST);

            Location location = locationQuery.getLocation();

            for (String locationField: Location.locationFields) {
                String locationValue = location.getValueOfField(locationField);
                if (locationValue != null && !locationValue.isEmpty()){
                    Query locQuery = new SimpleQueryParser(analyzer, locationField).parse(locationValue);
                    Query boostQuery = new BoostQuery(locQuery, locationQueryBoost);
                    booleanQueryBuilder.add(boostQuery, BooleanClause.Occur.SHOULD);
                }
            }

            query = booleanQueryBuilder.build();
        } else {
            // B - search just on the HOTEL and REVIEW fields if location search disabled
            query = hotelAndReviewTextsQuery;
        }

        IndexReader reader = DirectoryReader.open(hotelReviewIndexDirectory);
        IndexSearcher searcher = new IndexSearcher(reader);
        searcher.setSimilarity(retrievalModel);

        TopScoreDocCollector collector = TopScoreDocCollector.create(1000);  // MAX_RESULTS is just an int limiting the total number of hits
        int startIndex = (page -1) * hitsPerPage;  // our page is 1 based - so we need to convert to zero based

        // first step: get all Hotels of the current page for the query
        searcher.search(query, collector);
        TopDocs topDocs = collector.topDocs(startIndex, hitsPerPage);   // the pagination is done by the collector

        // save the total hits
        this.totalHits = collector.getTotalHits();

        ScoreDoc[] hits = topDocs.scoreDocs;

        for (ScoreDoc hit : hits) {
            Document d = searcher.doc(hit.doc);

            int hotelId = Integer.parseInt(d.get(HOTEL_ID));
            String hotelName = d.get(HOTEL_NAME);
            String hotelDescription = d.get(HOTEL_DESCRIPTION);
            String jsonFile = d.get(JSON_FILE);

            Hotel hotelEntity = new Hotel(hotelName, hotelDescription);
            hotelEntity.setHotelId(hotelId);
            hotelEntity.setJsonFile(jsonFile);

            topHotels.add(hotelEntity);
        }

        return topHotels;
    }

    /**
     * Get a list of review snippets for a given hotel and query string
     *
     * @param hotelId the hotel id to fetch
     * @param queryString the query string to search for
     * @return a list of snippets
     * @throws IOException
     * @throws ParseException
     * @throws InvalidTokenOffsetsException
     */
    public List<String> getTopReviewSnippetsOfHotel(int hotelId, String queryString) throws IOException, ParseException, InvalidTokenOffsetsException {

        List<String> reviewSnippets = new ArrayList<String>();

        if (queryString.isEmpty()) {
            return reviewSnippets;
        }

        StandardAnalyzer analyzer = new StandardAnalyzer(StopWordHelper.getHotelReviewStopWords());
        File reviewIndexDirectoryFile = new File(reviewIndexDirectory);
        Directory reviewIndexDirectory = FSDirectory.open(reviewIndexDirectoryFile.toPath());

        BooleanQuery.Builder booleanQueryBuilder = new BooleanQuery.Builder();

        Query idQuery = IntPoint.newExactQuery(HOTEL_ID_POINT, hotelId);

        String[] fields = {REVIEW_TITLE, REVIEW_TEXT};
        BooleanClause.Occur[] flags = {
                BooleanClause.Occur.SHOULD,
                BooleanClause.Occur.SHOULD,
        };
        Query textQuery = MultiFieldQueryParser.parse(queryString, fields, flags, analyzer);

        booleanQueryBuilder.add(idQuery, BooleanClause.Occur.MUST);
        booleanQueryBuilder.add(textQuery, BooleanClause.Occur.MUST);
        BooleanQuery booleanQuery = booleanQueryBuilder.build();

        IndexReader reader = DirectoryReader.open(reviewIndexDirectory);
        IndexSearcher searcher = new IndexSearcher(reader);
        searcher.setSimilarity(retrievalModel);

        // Instantiate HTMLFormatter & Highlighter
        SimpleHTMLFormatter htmlFormatter = new SimpleHTMLFormatter();
        Highlighter highlighter = new Highlighter(htmlFormatter, new QueryScorer(booleanQuery));
        TopDocs topDocsCounter = searcher.search(booleanQuery, 200);
        long length = Math.toIntExact(topDocsCounter.totalHits);

        TopDocs topDocs = searcher.search(booleanQuery, maxReviewSnippets);

        ScoreDoc[] hits = topDocs.scoreDocs;
        for (ScoreDoc hit : hits) {
            Document d = searcher.doc(hit.doc);
            String reviewText = d.get(REVIEW_TEXT);

            TokenStream tokenStream = TokenSources.getTokenStream(
                    reviewText,
                    searcher.getIndexReader().getTermVectors(hit.doc),
                    reviewText,
                    analyzer,
                    -1
            );
            String fragment = highlighter.getBestFragments(
                    tokenStream,
                    reviewText,
                    2,
                    " [...] "
            );
            if (!fragment.isEmpty()) {
                 reviewSnippets.add(fragment);
            }
        }
        return reviewSnippets;
    }

    /**
     *
     * @param hotel
     * @return Number of Reviews per Hotel
     * @throws IOException
     */
    private Integer getMaxReviewsProHotel(Hotel hotel) throws IOException {

        String jsonFile = hotel.getJsonFile();

        byte[] content = Files.readAllBytes(Paths.get(jsonFile));
        String fileContent = new String(content);   // eventuell muss hier das encoding noch angepasst werden

        Hotel resultHotel = new Gson().fromJson(fileContent, Hotel.class);
        return resultHotel.getReviews().size();
    }

    /**
     *
     * @param hotelId
     * @param queryString
     * @return actual Hits per Hotel for specified querystring
     * @throws IOException
     * @throws ParseException
     */
    private Integer getHitsInHotel(Integer hotelId, String queryString) throws IOException, ParseException {
        StandardAnalyzer analyzer = new StandardAnalyzer(StopWordHelper.getHotelReviewStopWords());
        Collection<Integer> hotelIds = new HashSet<>();
        hotelIds.add(hotelId);
        File hotelIndexDirectoryFile = new File(reviewIndexDirectory);
        Directory hotelIndexDirectory = FSDirectory.open(hotelIndexDirectoryFile.toPath());
        IndexReader reader = DirectoryReader.open(hotelIndexDirectory);
        IndexSearcher searcher = new IndexSearcher(reader);
        searcher.setSimilarity(retrievalModel);

        BooleanQuery.Builder booleanQueryBuilder = new BooleanQuery.Builder();

        Query idsQuery = IntPoint.newSetQuery(HOTEL_ID_POINT, hotelIds);
        /*String[] fields = {REVIEW_TITLE, REVIEW_TEXT};
        BooleanClause.Occur[] flags = {
                BooleanClause.Occur.SHOULD,
                BooleanClause.Occur.SHOULD,
        };*/
        String[] fields = {REVIEW_TEXT};
        BooleanClause.Occur[] flags = {
                BooleanClause.Occur.MUST
        };
        Query textQuery = MultiFieldQueryParser.parse(queryString, fields, flags, analyzer);

        booleanQueryBuilder.add(idsQuery, BooleanClause.Occur.MUST);
        booleanQueryBuilder.add(textQuery, BooleanClause.Occur.MUST);
        BooleanQuery booleanQuery = booleanQueryBuilder.build();

        TopDocs topDocs = searcher.search(booleanQuery, 200);
        long length = topDocs.totalHits;

        return Math.toIntExact(topDocs.totalHits);


    }

    /**
     * Get a list of ranked reviews for a hotel by its id and a query string
     *
     * @param hotelId the hotel id
     * @param queryString the query string
     * @param topCount the number of reviews to fetch
     * @return a list of review entities
     * @throws IOException
     * @throws ParseException
     */
    public List<ReviewEntity> getTopReviewsOfHotel(int hotelId, String queryString, int topCount) throws Exception {
        List<ReviewEntity> reviewList = new ArrayList<>();
        String queryStringUsed = queryString;

        if (queryStringUsed.isEmpty()) {
            return reviewList;
        }

        // get locations in the query
        LocationQuery locationQuery = null;

        if (locationSearchActicated){
            locationQuery = getLocationsInQuery(queryStringUsed);
            queryStringUsed = locationQuery.getEditedQueryString();
        }

        StandardAnalyzer analyzer = new StandardAnalyzer(StopWordHelper.getHotelReviewStopWords());
        File reviewIndexDirectoryFile = new File(reviewIndexDirectory);
        Directory reviewIndexDirectory = FSDirectory.open(reviewIndexDirectoryFile.toPath());

        BooleanQuery.Builder booleanQueryBuilder = new BooleanQuery.Builder();

        Query idQuery = IntPoint.newExactQuery(HOTEL_ID_POINT, hotelId);

        String[] fields = {REVIEW_TITLE, REVIEW_TEXT};
        BooleanClause.Occur[] flags = {
                BooleanClause.Occur.SHOULD,
                BooleanClause.Occur.SHOULD,
        };
        Query textQuery = MultiFieldQueryParser.parse(queryStringUsed, fields, flags, analyzer);

        booleanQueryBuilder.add(idQuery, BooleanClause.Occur.MUST);
        booleanQueryBuilder.add(textQuery, BooleanClause.Occur.MUST);
        BooleanQuery booleanQuery = booleanQueryBuilder.build();

        IndexReader reader = DirectoryReader.open(reviewIndexDirectory);
        IndexSearcher searcher = new IndexSearcher(reader);
        searcher.setSimilarity(retrievalModel);

        /* Highlighting-Part */
        Formatter formatter = new SimpleHTMLFormatter("<span class=\"highlight-term\">", "</span>");
        // It scores text fragments by the number of unique query terms found
        QueryScorer scorer = new QueryScorer(booleanQuery);
        // used to markup highlighted terms found in the best sections of a text
        Highlighter highlighter = new Highlighter(formatter, scorer);
        // It breaks text up into same-size texts but does not split up spans
        int fragmentSize = 10000;
        Fragmenter fragmenter = new SimpleSpanFragmenter(scorer, fragmentSize);
        highlighter.setTextFragmenter(fragmenter);
        TokenStream streamText;

        TopDocs topDocs = searcher.search(booleanQuery, 10);
        ScoreDoc[] hits = topDocs.scoreDocs;

        for (ScoreDoc hit : hits) {
            Document d = searcher.doc(hit.doc);
            String reviewText = d.get(REVIEW_TEXT);
            String reviewTitle = d.get(REVIEW_TITLE);
            String reviewId = d.get(REVIEW_ID);

            // Highlight only if the text is smaller than the fragment size of the highlighter
            if (reviewText.length() <= fragmentSize) {
                Fields termVectors = searcher.getIndexReader().getTermVectors(hit.doc);
                // Create token stream
                streamText = TokenSources.getTokenStream(
                        REVIEW_TEXT,
                        termVectors,
                        reviewText,
                        analyzer,
                        -1
                );

                // Get highlighted text fragment, possibly null
                String reviewTextHighlighted = highlighter.getBestFragment(streamText, reviewText);
                if (reviewTextHighlighted != null && reviewTextHighlighted.length() > reviewText.length()) {
                    reviewText = reviewTextHighlighted;
                }
            }

            ReviewEntity review = new ReviewEntity(reviewTitle, reviewText);
            review.setId(Integer.parseInt(reviewId));

            reviewList.add(review);
        }
        return reviewList;
    }

    /**
     * Obsolete Method. Just for compatibility to old Searching Method.
     * @param queryString
     * @return
     * @throws IOException
     * @throws ParseException
     * @throws InvalidTokenOffsetsException
     */
    @Deprecated
    @Override
    public Collection<SearchResult> getSearchResults(String queryString) throws IOException, ParseException, InvalidTokenOffsetsException {
        return getSearchResultsPage(queryString, 1, 5).getItemsOnPage();
    }
}
