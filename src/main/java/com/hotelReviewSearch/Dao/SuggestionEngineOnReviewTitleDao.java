package com.hotelReviewSearch.Dao;

import com.hotelReviewSearch.Config.ConfigInterface;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.*;
import org.apache.lucene.search.spell.Dictionary;
import org.apache.lucene.search.suggest.DocumentDictionary;
import org.apache.lucene.search.suggest.Lookup;
import org.apache.lucene.search.suggest.analyzing.AnalyzingSuggester;
import org.apache.lucene.search.suggest.analyzing.FuzzySuggester;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.nio.CharBuffer;
import java.util.List;

@Repository
@Qualifier("reviewTitleSuggestions")
public class SuggestionEngineOnReviewTitleDao implements SuggestionDaoInterface, ConfigInterface {

    public static final int RESULTS_TO_DISPLAY = 10;
    private AnalyzingSuggester suggester;

    public SuggestionEngineOnReviewTitleDao() throws IOException {
        setSuggester(buildNewSuggestor());
    }

    @Override
    public String[] getSuggestions(String queryString){

        String[] autosuggestResults;

        try {
            List<Lookup.LookupResult> results = getSuggester().lookup(CharBuffer.wrap(queryString), false, RESULTS_TO_DISPLAY);

            autosuggestResults = new String[results.size()];
            for(int i=0; i < results.size(); i++) {
                Lookup.LookupResult result = results.get(i);
                autosuggestResults[i] = result.key.toString();
            }

            return autosuggestResults;

        } catch (IOException ex) {
            return new String[0];
        }
    }

    /**
     * Creates a suggester based on a LuceneIndexDictionary.
     * No Term Weights are considered.
     * The full ReviewTitle-Field is the base for the suggestions
     * @return true if builded successfully
     */
    @Override
    public AnalyzingSuggester buildNewSuggestor() throws IOException {
        AnalyzingSuggester newSuggester = new FuzzySuggester(new RAMDirectory(), "fuzzy", new StandardAnalyzer());
        File hotelReviewIndexDirectoryFile = new File(hotelReviewIndexDirectory);
        Directory hotelReviewIndexDirectory = FSDirectory.open(hotelReviewIndexDirectoryFile.toPath());
        IndexReader reader = DirectoryReader.open(hotelReviewIndexDirectory);

        Dictionary dict = new DocumentDictionary(reader, REVIEW_TITLE, REVIEW_TITLE);
        newSuggester.build(dict);
        return newSuggester;
    }

    @Override
    public void setSuggester(AnalyzingSuggester suggester) {
        this.suggester = suggester;
    }

    public AnalyzingSuggester getSuggester() {
        return suggester;
    }
}
