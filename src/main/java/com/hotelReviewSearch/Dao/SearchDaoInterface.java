package com.hotelReviewSearch.Dao;

import com.hotelReviewSearch.Entity.Location;
import com.hotelReviewSearch.Entity.Page;
import com.hotelReviewSearch.Entity.ReviewEntity;
import com.hotelReviewSearch.Entity.SearchResult;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

public interface SearchDaoInterface {

    int maxReviewSnippets = 3;

    @Deprecated
    Collection<SearchResult> getSearchResults(String queryString) throws IOException, ParseException, InvalidTokenOffsetsException;

    Page<SearchResult> getSearchResultsPage(String queryString, int page, int hitsPerPage) throws IOException, ParseException, InvalidTokenOffsetsException;

    List<ReviewEntity> getTopReviewsOfHotel(int hotelId, String queryString, int topCount) throws Exception;
}
