package com.hotelReviewSearch.Dao;

import com.hotelReviewSearch.Entity.Hotel;
import java.io.IOException;

public interface HotelDaoInterface {
    Hotel getHotelById(int hotelId) throws IOException;
    Hotel getHotelWithTopReviewsById(int hotelId, String queryString, int topCount) throws Exception;
}
