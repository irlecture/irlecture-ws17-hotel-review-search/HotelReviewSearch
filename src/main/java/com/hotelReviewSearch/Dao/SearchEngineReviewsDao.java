package com.hotelReviewSearch.Dao;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hotelReviewSearch.Config.ConfigInterface;
import com.hotelReviewSearch.Entity.*;
import com.hotelReviewSearch.Helper.StopWordHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.IntPoint;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.*;
import org.apache.lucene.search.highlight.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Repository
@Qualifier("liveReviewData")
public class SearchEngineReviewsDao implements SearchDaoInterface, ConfigInterface, HotelDaoInterface
{
    private final Logger logger = LogManager.getLogger(SearchEngineDao.class);

    @Override
    public Collection<SearchResult> getSearchResults(String queryString) throws IOException, ParseException, InvalidTokenOffsetsException{
        ArrayList<SearchResult> searchResults = new ArrayList<>();

        if (queryString.isEmpty()) {
            return searchResults;
        }

        StandardAnalyzer analyzer = new StandardAnalyzer(StopWordHelper.getHotelReviewStopWords());
        File reviewIndexDirectoryFile = new File(reviewIndexDirectory);
        Directory reviewIndexDirectory = FSDirectory.open(reviewIndexDirectoryFile.toPath());

        // Fields to search on
        String[] reviewFields = {REVIEW_TITLE, REVIEW_TEXT};
        BooleanClause.Occur[] reviewFlags = {
                BooleanClause.Occur.SHOULD,
                BooleanClause.Occur.SHOULD
        };
        Query query = MultiFieldQueryParser.parse(queryString, reviewFields, reviewFlags, analyzer);
        IndexReader reader = DirectoryReader.open(reviewIndexDirectory);
        IndexSearcher searcher = new IndexSearcher(reader);

        // Instantiate HTMLFormatter & Highlighter
        SimpleHTMLFormatter htmlFormatter = new SimpleHTMLFormatter();
        Highlighter highlighter = new Highlighter(htmlFormatter, new QueryScorer(query));

        // Get 500 best ranked reviews
        TopDocs topDocs = searcher.search(query, 500);
        ScoreDoc[] hits = topDocs.scoreDocs;

        logger.info("Found [" + topDocs.totalHits + "] total hits in reviews for query [" + queryString + "].");

        ArrayList<Integer> hotelIds = new ArrayList<>();

        // Remember each hotel id in the correct order
        for (int i = 0; i < hits.length; i++) {
            int docId = hits[i].doc;
            Document d = searcher.doc(docId);

            hotelIds.add(i, Integer.parseInt(d.get(HOTEL_ID)));
        }

        // Get all hotels for the reviews
        HashMap<Integer, Hotel> hotelHashMap = searchHotelsByHotelIds(hotelIds);
        // A mapping for a hotel id to its index in the search result list
        HashMap<Integer, Integer> hotelIdSearchResultListMapping = new HashMap<>();

        for (ScoreDoc hit : hits) {
            SearchResult searchResult;
            Document d = searcher.doc(hit.doc);

            int hotelId = Integer.parseInt(d.get(HOTEL_ID));
            String reviewTitle = d.get(REVIEW_TITLE);
            String reviewText = d.get(REVIEW_TEXT);
            ReviewEntity reviewEntity = new ReviewEntity(reviewTitle, reviewText);

            if (hotelIdSearchResultListMapping.containsKey(hotelId)) {
                // The hotel id already exists in the mapping, therefore get the result object and add the review
                int hotelListIndex = hotelIdSearchResultListMapping.get(hotelId);
                searchResult = searchResults.get(hotelListIndex);
                Hotel hotel = searchResult.getHotel();
                hotel.setReview_count(getHitsInHotel(hotelId,queryString));
                searchResult.setHitCountInHotel(hotel.getReview_count());
                searchResult.setMaxCountReviews(getMaxReviewsProHotel(hotelId));
                hotel.addReview(reviewEntity);
            } else {
                // The hotel does not exist in the mapping, therefore create a new one and add it to the mapping
                Hotel hotel = hotelHashMap.get(hotelId);
                hotel.setReview_count(getHitsInHotel(hotelId,queryString));
                hotel.addReview(reviewEntity);
                searchResult = new SearchResult(hotel);
                searchResult.setHitCountInHotel(hotel.getReview_count());
                searchResult.setMaxCountReviews(getMaxReviewsProHotel(hotelId));
                searchResults.add(searchResult);
                hotelIdSearchResultListMapping.put(hotelId, searchResults.indexOf(searchResult));
            }

            // Check if there are already 3 snippets for a hotel, if not create one
            if (searchResult.getReviewSnippetCount() < maxReviewSnippets) {
                TokenStream tokenStream = TokenSources.getTokenStream(
                        reviewText,
                        searcher.getIndexReader().getTermVectors(hit.doc),
                        reviewText,
                        analyzer,
                        -1
                );
                String fragment = highlighter.getBestFragments(
                        tokenStream,
                        reviewText,
                        2,
                        " [...] "
                );
                if (!fragment.isEmpty()) {
                    searchResult.addReviewSnippet(fragment);
                }
            }
        }

        return searchResults;
    }

    /**
     * This is an old SearchEngine that does not implement lucene-side search pagination.
     */
    @Override
    public Page<SearchResult> getSearchResultsPage(String queryString, int page, int hitsPerPage) throws IOException, ParseException, InvalidTokenOffsetsException {
        throw new NotImplementedException();
    }

    /**
     * This is an old SearchEngine that does not implement lucene-side search pagination.
     */
    @Override
    public List<ReviewEntity> getTopReviewsOfHotel(int hotelId, String queryString, int topCount) throws Exception {
        throw new NotImplementedException();
    }

    /**
     * Get a single hotel entity from the hotel index by a given hotel id.
     * @param hotelId the id to search for
     * @return the hotel entity
     * @throws IOException
     */
    public Hotel getHotelById(int hotelId) throws IOException {
        Collection<Integer> hotelIds = new ArrayList<Integer>();
        hotelIds.add(hotelId);
        String jsonFile = searchHotelsByHotelIds(hotelIds).get(hotelId).getJsonFile();

        byte[] content = Files.readAllBytes(Paths.get(jsonFile));
        String fileContent = new String(content);   // eventuell muss hier das encoding noch angepasst werden

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        Gson gson = gsonBuilder.create();

        Hotel resultHotel = gson.fromJson(fileContent, Hotel.class);

        return resultHotel;
    }

    private Integer getMaxReviewsProHotel(int hotelId) throws IOException {
        Collection<Integer> hotelIds = new ArrayList<Integer>();
        hotelIds.add(hotelId);
        String jsonFile = searchHotelsByHotelIds(hotelIds).get(hotelId).getJsonFile();

        byte[] content = Files.readAllBytes(Paths.get(jsonFile));
        String fileContent = new String(content);   // eventuell muss hier das encoding noch angepasst werden

        Hotel resultHotel = new Gson().fromJson(fileContent, Hotel.class);
        return resultHotel.getReviews().size();
    }

    @Override
    public Hotel getHotelWithTopReviewsById(int hotelId, String queryString, int topCount) throws Exception {
        throw new NotImplementedException();
    }

    /**
     * Get all hotel entities from the hotel index by a given list of hotel ids
     * @param hotelIds the ids to search for
     * @return a hashmap with the hotel-id as key and the related hotel entity as value
     * @throws IOException
     */
    private HashMap<Integer, Hotel> searchHotelsByHotelIds(Collection<Integer> hotelIds) throws IOException {
        HashMap<Integer, Hotel> hotelHashMap = new HashMap<>();
        if (hotelIds.isEmpty()) {
            return hotelHashMap;
        }
        File hotelIndexDirectoryFile = new File(hotelIndexDirectory);
        Directory hotelIndexDirectory = FSDirectory.open(hotelIndexDirectoryFile.toPath());

        // Create a new set query to search for a set of values
        Query setQuery = IntPoint.newSetQuery(HOTEL_ID_POINT, hotelIds);

        IndexReader reader = DirectoryReader.open(hotelIndexDirectory);
        IndexSearcher searcher = new IndexSearcher(reader);

        TopDocs topDocs = searcher.search(setQuery, hotelIds.size());
        ScoreDoc[] hits = topDocs.scoreDocs;

        for (ScoreDoc hit : hits) {
            int docId = hit.doc;
            Document d = searcher.doc(docId);
            Hotel hotel = new Hotel(d.get(HOTEL_NAME), d.get(HOTEL_DESCRIPTION));
            hotel.setJsonFile(d.get(JSON_FILE));
            hotel.setHotelId(Integer.parseInt(d.get(HOTEL_ID)));
            hotelHashMap.put(hotel.getHotelId(), hotel);
        }

        return hotelHashMap;
    }

    private Map<Integer, Integer> countReviewsProHotel(Collection<Integer> hotelIds) throws IOException {
        //int counter = 0;
        Map<Integer, Integer> hotelCountById = new HashMap<Integer, Integer>();
        if (hotelIds.isEmpty()) {
            return (HashMap<Integer, Integer>) hotelCountById;
        }
        for (Integer hotelId : hotelIds) {
            if(hotelIds.contains(hotelId)) {
                hotelCountById.put(hotelId, hotelCountById.containsKey(hotelId)?hotelCountById.get(hotelId)+1:1);
            }
        }
        return (HashMap<Integer, Integer>) hotelCountById;
    }

    private Integer getHitsInHotel(Integer hotelId, String queryString) throws IOException, ParseException {
        StandardAnalyzer analyzer = new StandardAnalyzer(StopWordHelper.getHotelReviewStopWords());
        Collection<Integer> hotelIds = new HashSet<>();
        hotelIds.add(hotelId);
        File hotelIndexDirectoryFile = new File(reviewIndexDirectory);
        Directory hotelIndexDirectory = FSDirectory.open(hotelIndexDirectoryFile.toPath());
        IndexReader reader = DirectoryReader.open(hotelIndexDirectory);
        IndexSearcher searcher = new IndexSearcher(reader);

        BooleanQuery.Builder booleanQueryBuilder = new BooleanQuery.Builder();

        Query idsQuery = IntPoint.newSetQuery(HOTEL_ID_POINT, hotelIds);
        /*String[] fields = {REVIEW_TITLE, REVIEW_TEXT};
        BooleanClause.Occur[] flags = {
                BooleanClause.Occur.SHOULD,
                BooleanClause.Occur.SHOULD,
        };*/
        String[] fields = {REVIEW_TEXT};
        BooleanClause.Occur[] flags = {
                BooleanClause.Occur.MUST
        };
        Query textQuery = MultiFieldQueryParser.parse(queryString, fields, flags, analyzer);

        booleanQueryBuilder.add(idsQuery, BooleanClause.Occur.MUST);
        booleanQueryBuilder.add(textQuery, BooleanClause.Occur.MUST);
        BooleanQuery booleanQuery = booleanQueryBuilder.build();

        TopDocs topDocs = searcher.search(booleanQuery, 200);
        long length = topDocs.totalHits;

        return Math.toIntExact(topDocs.totalHits);
    }


}
