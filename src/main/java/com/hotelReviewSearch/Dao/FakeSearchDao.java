package com.hotelReviewSearch.Dao;

import com.hotelReviewSearch.Entity.*;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.*;

@Repository
@Qualifier("fakeData")
public class FakeSearchDao implements SearchDaoInterface {

    private static Map<Integer, SearchResult> searchResults;

    static {

        String loremIpsum = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";

        searchResults = new HashMap<Integer, SearchResult>(){
            {
                put(1, new SearchResult(new Hotel("Leipziger 5 Sterne Ressort", "Ein super schönes Hotel. " + loremIpsum).addReview(new ReviewEntity("Gut gefallen", "Das Hotel hat mir sehr gut gefallen ..."))));
                put(2, new SearchResult(new Hotel("Dresdener Kaschemme", "Nicht so toll. "  + loremIpsum)));
                put(3, new SearchResult(new Hotel("Berliner Bruchbude", "Nicht zu empfehlen. "  + loremIpsum)));
                put(4, new SearchResult(new Hotel("Münchener Bruchbude", "Nicht zu empfehlen. "  + loremIpsum)));
                put(5, new SearchResult(new Hotel("Hamburger Bruchbude", "Nicht zu empfehlen. "  + loremIpsum)));
                put(6, new SearchResult(new Hotel("Kölner Bruchbude", "Nicht zu empfehlen. "  + loremIpsum)));
                put(7, new SearchResult(new Hotel("Frankfurter Bruchbude", "Nicht zu empfehlen. "  + loremIpsum)));
                put(8, new SearchResult(new Hotel("Nürnberger Bruchbude", "Nicht zu empfehlen. "  + loremIpsum)));
                put(9, new SearchResult(new Hotel("Dortmunder Bruchbude", "Nicht zu empfehlen. "  + loremIpsum)));
                put(10, new SearchResult(new Hotel("Mainzer Bruchbude", "Nicht zu empfehlen. "  + loremIpsum)));
                put(11, new SearchResult(new Hotel("Augsburger Bruchbude", "Nicht zu empfehlen. "  + loremIpsum)));
                put(12, new SearchResult(new Hotel("Gladbacher Bruchbude", "Nicht zu empfehlen. "  + loremIpsum)));
                put(13, new SearchResult(new Hotel("Stuttgarter Bruchbude", "Nicht zu empfehlen. "  + loremIpsum)));
            }
        };
    }

    @Override
    public Collection<SearchResult> getSearchResults(String queryString) {
        return FakeSearchDao.searchResults.values();
    }

    @Override
    public Page<SearchResult> getSearchResultsPage(String queryString, int page, int hitsPerPage) throws IOException, ParseException, InvalidTokenOffsetsException {

        Collection<SearchResult> results = new ArrayList<>();
        int start = ((page - 1) * hitsPerPage) + 1;
        int end = start + hitsPerPage - 1;
        for (int i = start; i <= end; i++){
            results.add(searchResults.get(i));
        }

        return new Page<SearchResult>(results, page, hitsPerPage, searchResults.size());
    }

    @Override
    public List<ReviewEntity> getTopReviewsOfHotel(int hotelId, String queryString, int topCount) throws Exception {
        List<ReviewEntity> reviewEntityList = new ArrayList<ReviewEntity>();
        reviewEntityList.add(new ReviewEntity("Gut gefallen", "alles top"));
        return reviewEntityList;
    }
}
