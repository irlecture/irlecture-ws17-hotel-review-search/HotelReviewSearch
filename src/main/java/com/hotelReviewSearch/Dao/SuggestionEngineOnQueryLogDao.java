package com.hotelReviewSearch.Dao;

import com.hotelReviewSearch.Config.ConfigInterface;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.spell.Dictionary;
import org.apache.lucene.search.suggest.DocumentDictionary;
import org.apache.lucene.search.suggest.Lookup;
import org.apache.lucene.search.suggest.analyzing.AnalyzingSuggester;
import org.apache.lucene.search.suggest.analyzing.FuzzySuggester;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.nio.CharBuffer;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Repository
@Qualifier("suggestOnQueryLog")
public class SuggestionEngineOnQueryLogDao implements SuggestionDaoInterface, ConfigInterface {

    public static final int RESULTS_TO_DISPLAY = 5;
    private AnalyzingSuggester suggester;
    /**
     * reentrant mutual exclusion Lock to have a save access to the suggestor
     */
    private final Lock lockSuggester = new ReentrantLock();

    public SuggestionEngineOnQueryLogDao() throws IOException {
        setSuggester(buildNewSuggestor());
    }

    @Override
    public String[] getSuggestions(String queryString){

        String[] autosuggestResults;

        try {
            AnalyzingSuggester currentSuggestor = getSuggester();
            List<Lookup.LookupResult> results = currentSuggestor.lookup(CharBuffer.wrap(queryString), false, RESULTS_TO_DISPLAY);

            autosuggestResults = new String[results.size() + 1];
            for(int i=0; i < results.size(); i++) {
                Lookup.LookupResult result = results.get(i);
                autosuggestResults[i] = result.key.toString();
            }
            // always add an empty result add the end because of bug in Bloodhound Suggestion Engine
            // it doesn't work, when the number of returned results matches the number of displayed results
            autosuggestResults[results.size()] = "";

            return autosuggestResults;

        } catch (IOException ex) {
            return new String[0];
        }
    }

    /**
     * Creates a suggester based on a LuceneIndexDictionary for the logged Queries.
     * No Term Weights are considered.
     */
    @Override
    public AnalyzingSuggester buildNewSuggestor() throws IOException {
        AnalyzingSuggester newSuggester = new FuzzySuggester(new RAMDirectory(), "fql", new StandardAnalyzer());

        File requestIndexDirectoryFile = new File(requestIndexDirectory);
        Directory searchRequestIndexDirectory = FSDirectory.open(requestIndexDirectoryFile.toPath());
        IndexReader reader = DirectoryReader.open(searchRequestIndexDirectory);

        Dictionary dict = new DocumentDictionary(reader, SEARCH_REQUEST, SEARCH_REQUEST);

        newSuggester.build(dict);
        reader.close();

        return newSuggester;
    }

    public AnalyzingSuggester getSuggester() {
        return suggester;
    }

    @Override
    public void setSuggester(AnalyzingSuggester suggester) {
        this.suggester = suggester;
    }

    public Lock getLockSuggester() {
        return lockSuggester;
    }
}
