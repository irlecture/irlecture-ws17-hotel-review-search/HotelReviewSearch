package com.hotelReviewSearch.Dao;

import com.hotelReviewSearch.Config.ConfigInterface;
import com.hotelReviewSearch.Helper.SuggestionIterator;
import com.hotelReviewSearch.Helper.SuggestionTerm;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.*;
import org.apache.lucene.search.spell.Dictionary;
import org.apache.lucene.search.suggest.DocumentDictionary;
import org.apache.lucene.search.suggest.Lookup;
import org.apache.lucene.search.suggest.analyzing.AnalyzingSuggester;
import org.apache.lucene.search.suggest.analyzing.FuzzySuggester;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.BytesRef;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This Suggestion Engine combines the suggestions from ReviewText-TermFrequencies and the QueryLog.
 */
@Repository
@Qualifier("suggestOnTermFrequencyAndQueryLog")
public class SuggestionEngineTfAndQlDao implements SuggestionDaoInterface, ConfigInterface {

    public static final int RESULTS_FROM_TERM_FREQUENCY = 5;
    public static final int RESULTS_FROM_QUERY_LOG = 5;
    private AnalyzingSuggester termFrequencySuggester;
    private AnalyzingSuggester queryLogSuggester;

    public SuggestionEngineTfAndQlDao() throws IOException {
        setSuggester(buildNewSuggestor());
        setTermFrequencySuggester(buildNewTermFrequencySuggestor());
    }


    @Override
    public String[] getSuggestions(String queryString){

        try {
            // search for the first X suggestion with the queryString in the queryLogIndex
            List<Lookup.LookupResult> qlResults = getSuggester().lookup(CharBuffer.wrap(queryString), false, RESULTS_FROM_QUERY_LOG);
            Set<String> qlResultSet = new HashSet<String>();
            for (Lookup.LookupResult qlResult: qlResults) {
                qlResultSet.add(qlResult.key.toString());
            }

            // search for the other Y suggestion with the last term of the query in the termFrequencyIterator
            String[] queryTerms = queryString.split("\\s+");    // all whitespaces as delemitter
            String lookupTerm = queryString;
            String preTerms = "";

            // if search over terms, search only for the last term
            // set the other terms as preTerms
            if (queryTerms.length > 1){
                lookupTerm = queryTerms[queryTerms.length - 1];
                for (int i = 0; i < queryTerms.length - 1; i++){
                    preTerms += queryTerms[i] + " ";
                }
            }
            // there are more results than necessary returned for tfResults, because there could be duplicates in both result sets
            List<Lookup.LookupResult> tfResults = getTermFrequencySuggester().lookup(CharBuffer.wrap(lookupTerm), false, RESULTS_FROM_TERM_FREQUENCY);

            List<String> autosuggestResults = new ArrayList<String>();

            // first X results: queryLog-Results
            for (Lookup.LookupResult qlResult: qlResults) {
                autosuggestResults.add(qlResult.key.toString());
            }
            // second Y results: termFrequency-Results
            for (Lookup.LookupResult tfResult : tfResults) {
                String suggestionText = preTerms + tfResult.key.toString();   // preTerms added to suggestion
                autosuggestResults.add(suggestionText);

                if (qlResultSet.contains(suggestionText)){
                    // be sure there are no duplicates
                    qlResults.remove(suggestionText);
                }
            }

            // always add an empty result add the end because of bug in Bloodhound Suggestion Engine
            // it doesn't work, when the number of returned results matches the number of displayed results
            autosuggestResults.add("");

            return autosuggestResults.toArray(new String[0]);

        } catch (IOException ex) {
            return new String[0];
        }
    }

    @Override
    public AnalyzingSuggester buildNewSuggestor() throws IOException {
        AnalyzingSuggester newQlSuggester = new FuzzySuggester(new RAMDirectory(), "qls", new StandardAnalyzer());
        File requestIndexDirectoryFile = new File(requestIndexDirectory);
        Directory searchRequestIndexDirectory = FSDirectory.open(requestIndexDirectoryFile.toPath());
        IndexReader reader = DirectoryReader.open(searchRequestIndexDirectory);

        Dictionary dict = new DocumentDictionary(reader, SEARCH_REQUEST, SEARCH_REQUEST);

        newQlSuggester.build(dict);
        reader.close();
        return newQlSuggester;
    }

    /**
     * Gets all Terms of the index and their frequency.
     * Creates a suggester based on these terms and their weights.
     * @return
     */
    public AnalyzingSuggester buildNewTermFrequencySuggestor() throws IOException {
        AnalyzingSuggester newTfSuggester = new FuzzySuggester(new RAMDirectory(), "tfs", new StandardAnalyzer());
        List<SuggestionTerm> suggestionTerms = new ArrayList<SuggestionTerm>();

        File hotelReviewIndexDirectoryFile = new File(hotelReviewIndexDirectory);
        Directory hotelReviewIndexDirectory = FSDirectory.open(hotelReviewIndexDirectoryFile.toPath());
        IndexReader reader = DirectoryReader.open(hotelReviewIndexDirectory);

        Fields fields = MultiFields.getFields(reader);
        Terms terms = fields.terms(REVIEW_TEXT);
        TermsEnum termsEnum = terms.iterator();

        BytesRef currentTerm = termsEnum.next();
        while (currentTerm != null) {
            String term = currentTerm.utf8ToString();
            long freq = reader.totalTermFreq(new Term(REVIEW_TEXT, currentTerm));
            suggestionTerms.add(new SuggestionTerm(term, freq));
            currentTerm = termsEnum.next();
        }

        newTfSuggester.build(new SuggestionIterator(suggestionTerms.iterator()));

        return newTfSuggester;
    }

    @Override
    public void setSuggester(AnalyzingSuggester suggester) {
        this.queryLogSuggester = suggester;
    }

    public AnalyzingSuggester getSuggester() {
        return queryLogSuggester;
    }

    public AnalyzingSuggester getTermFrequencySuggester() {
        return termFrequencySuggester;
    }

    public void setTermFrequencySuggester(AnalyzingSuggester termFrequencySuggester) {
        this.termFrequencySuggester = termFrequencySuggester;
    }
}
