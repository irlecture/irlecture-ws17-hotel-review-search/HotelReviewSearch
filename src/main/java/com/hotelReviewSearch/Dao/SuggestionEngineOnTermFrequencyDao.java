package com.hotelReviewSearch.Dao;

import com.hotelReviewSearch.Config.ConfigInterface;
import com.hotelReviewSearch.Helper.SuggestionIterator;
import com.hotelReviewSearch.Helper.SuggestionTerm;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.*;
import org.apache.lucene.search.suggest.Lookup;
import org.apache.lucene.search.suggest.analyzing.AnalyzingSuggester;
import org.apache.lucene.search.suggest.analyzing.FuzzySuggester;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.BytesRef;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.List;

@Repository
@Qualifier("suggestOnTermFrequency")
public class SuggestionEngineOnTermFrequencyDao implements SuggestionDaoInterface, ConfigInterface {

    public static final int RESULTS_TO_DISPLAY = 5;
    private AnalyzingSuggester suggester;

    public SuggestionEngineOnTermFrequencyDao() throws IOException {
        setSuggester(buildNewSuggestor());
    }

    @Override
    public String[] getSuggestions(String queryString){

        String[] autosuggestResults;

        try {
            String[] queryTerms = queryString.split("\\s+");    // all whitespaces as delemitter
            String lookupTerm = queryString;
            String preTerms = "";

            // if search over terms, search only for the last term
            // set the other terms as preTerms
            if (queryTerms.length > 1){
                lookupTerm = queryTerms[queryTerms.length - 1];
                for (int i = 0; i < queryTerms.length - 1; i++){

                    List<Lookup.LookupResult> preTermResults = suggester.lookup(CharBuffer.wrap(queryTerms[i]), false, 1);
                    if (preTermResults.size() > 0){
                        // try to get a suggestion for the preTerm
                        preTerms += preTermResults.get(0).key.toString() + " ";
                    } else {
                        // otherwise take the userinput
                        preTerms += queryTerms[i] + " ";
                    }
                }
            }
            // there are more results than necessary returned for tfResults, because there could be duplicates in both result sets
            List<Lookup.LookupResult> results = suggester.lookup(CharBuffer.wrap(lookupTerm), false, RESULTS_TO_DISPLAY);

            autosuggestResults = new String[results.size() + 1];
            for(int i=0; i < results.size(); i++) {
                Lookup.LookupResult result = results.get(i);
                autosuggestResults[i] = preTerms + result.key.toString();
            }
            // always add an empty result add the end because of bug in Bloodhound Suggestion Engine
            // it doesn't work, when the number of returned results matches the number of displayed results
            autosuggestResults[results.size()] = "";

            return autosuggestResults;

        } catch (IOException ex) {
            return new String[0];
        }
    }

    /**
     * Gets all Terms of the index and their frequency.
     * Creates a suggester based on these terms and their weights.
     * @return
     */
    @Override
    public AnalyzingSuggester buildNewSuggestor() throws IOException {
        AnalyzingSuggester newSuggester = new FuzzySuggester(new RAMDirectory(), "tfql", new StandardAnalyzer());

        List<SuggestionTerm> suggestionTerms = new ArrayList<SuggestionTerm>();

        File hotelReviewIndexDirectoryFile = new File(hotelReviewIndexDirectory);
        Directory hotelReviewIndexDirectory = FSDirectory.open(hotelReviewIndexDirectoryFile.toPath());
        IndexReader reader = DirectoryReader.open(hotelReviewIndexDirectory);

        Fields fields = MultiFields.getFields(reader);
        Terms terms = fields.terms(REVIEW_TEXT);
        TermsEnum termsEnum = terms.iterator();

        BytesRef currentTerm = termsEnum.next();
        while (currentTerm != null) {
            String term = currentTerm.utf8ToString();
            long freq = reader.totalTermFreq(new Term(REVIEW_TEXT, currentTerm));
            suggestionTerms.add(new SuggestionTerm(term, freq));
            currentTerm = termsEnum.next();
        }

        newSuggester.build(new SuggestionIterator(suggestionTerms.iterator()));

        return newSuggester;
    }

    @Override
    public void setSuggester(AnalyzingSuggester suggester) {
        this.suggester = suggester;
    }

    public AnalyzingSuggester getSuggester() {
        return suggester;
    }
}
