package com.hotelReviewSearch.Dao;

import org.apache.lucene.search.suggest.analyzing.AnalyzingSuggester;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.io.IOException;

/**
 * Data Access Object for Fake Suggestions.
 */
@Repository
@Qualifier("fakeSuggestions")
public class FakeSuggestionDao implements SuggestionDaoInterface {

    private static String[] suggestions;

    static {
        String[] temp = { "nice cheep hotel", "childfriendly", "childfriendly dogfriendly" };
        suggestions = temp;
    }

    /**
     * Returns some static fake suggestions.
     * @param queryString The query to make suggestions for.
     * @return String-Array of suggestions.
     */
    @Override
    public String[] getSuggestions(String queryString) {
        return FakeSuggestionDao.suggestions;
    }

    @Override
    public AnalyzingSuggester buildNewSuggestor() throws IOException {
        return null;
    }

    @Override
    public void setSuggester(AnalyzingSuggester suggester) {
    }
}
