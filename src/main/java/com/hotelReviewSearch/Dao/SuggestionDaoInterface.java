package com.hotelReviewSearch.Dao;

import org.apache.lucene.search.suggest.analyzing.AnalyzingSuggester;

import java.io.IOException;

public interface SuggestionDaoInterface {

    /**
     * Get Suggestions for the requested query.
     * @param queryString The query to give suggestions for.
     * @return String-Array of suggestions.
     */
    String[] getSuggestions(String queryString);

    AnalyzingSuggester buildNewSuggestor() throws IOException;

    void setSuggester(AnalyzingSuggester suggester);
}
