package com.hotelReviewSearch.Dao;

import com.hotelReviewSearch.Config.ConfigInterface;
import com.hotelReviewSearch.Entity.*;
import com.hotelReviewSearch.Helper.StopWordHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.*;
import org.apache.lucene.search.highlight.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Repository
@Qualifier("liveData")
public class SearchEngineDao implements SearchDaoInterface, ConfigInterface
{
    private static final Logger logger = LogManager.getLogger(SearchEngineDao.class);

    @Override
    public Collection<SearchResult> getSearchResults(String queryString) throws IOException, ParseException, InvalidTokenOffsetsException {
        ArrayList<SearchResult> searchResultArrayList = new ArrayList<>();

        if (queryString.isEmpty()) {
            return searchResultArrayList;
        }

        StandardAnalyzer analyzer = new StandardAnalyzer(StopWordHelper.getHotelReviewStopWords());

        // Get the index directory
        File indexDirectoryFile = new File(indexDirectory);
        Directory index = FSDirectory.open(indexDirectoryFile.toPath());
        //Fields to search on
        String [] fields = {HOTEL_NAME, HOTEL_DESCRIPTION, REVIEW_TITLE, REVIEW_TEXT};
        BooleanClause.Occur[] flags = {
                BooleanClause.Occur.SHOULD,
                BooleanClause.Occur.SHOULD,
                BooleanClause.Occur.SHOULD,
                BooleanClause.Occur.SHOULD
        };

        Query query = MultiFieldQueryParser.parse(queryString, fields, flags, analyzer);
        IndexReader reader = DirectoryReader.open(index);
        IndexSearcher searcher = new IndexSearcher(reader);

        TopDocs topDocs = searcher.search(query, 100);
        ScoreDoc[] hits = topDocs.scoreDocs;
        logger.info("Found [" + topDocs.totalHits + "] total hits for query [" + queryString + "].");

        //instantiate HTMLFormatter & Highlighter
        SimpleHTMLFormatter htmlFormatter = new SimpleHTMLFormatter();
        Highlighter highlighter = new Highlighter(htmlFormatter, new QueryScorer(query));
        //iterate over hits
        for (int i=0; i < hits.length; i++) {
            int docId = hits[i].doc;
            Document d = searcher.doc(docId);

            String hotelName = d.get(HOTEL_NAME);
            String hotelDescription = d.get(HOTEL_DESCRIPTION);
            Hotel hotelEntity = new Hotel(hotelName, hotelDescription);

            String[] reviewTitles = d.getValues(REVIEW_TITLE);
            String[] reviewTexts = d.getValues(REVIEW_TEXT);

            if (reviewTitles.length != reviewTexts.length) {
                //todo: handle error!
            }

            SearchResult searchResult = new SearchResult(hotelEntity);
            // Fill original titles and reviews in the hotel entity ... not clear if this is necessary
            for (int j = 0; j < reviewTitles.length; j++) {
                ReviewEntity tempReview = new ReviewEntity(reviewTitles[j], reviewTexts[j]);
                hotelEntity.addReview(tempReview);
                if (searchResult.getReviewSnippetList().size() < maxReviewSnippets) {
                    //create TokenStream for highlighting searchquery and split into fragments
                    TokenStream tokenStream = TokenSources.getTokenStream(
                            d.getValues(REVIEW_TEXT)[j],
                            searcher.getIndexReader().getTermVectors(docId),
                            d.getValues(REVIEW_TEXT)[j],
                            analyzer,
                            -1
                    );
                    String[] fragments = highlighter.getBestFragments(tokenStream, d.getValues(REVIEW_TEXT)[j], maxReviewSnippets);
                    for (String fragment : fragments) {
                        //add fragment as reviewSnippet
                        searchResult.addReviewSnippet(fragment);
                    }
                }
            }

            //todo: set hit count in the search result ... think it is a cool information, how many hits inside a hotel
            //todo: retrieve best matching reviews -> create new Index
            searchResultArrayList.add(searchResult);
        }
        return searchResultArrayList;
    }

    /**
     * This is an old SearchEngine that does not implement lucene-side search pagination.
     */
    @Override
    public Page<SearchResult> getSearchResultsPage(String queryString, int page, int hitsPerPage) throws IOException, ParseException, InvalidTokenOffsetsException {
        throw new NotImplementedException();
    }

    /**
     * This is an old SearchEngine that does not implement lucene-side search pagination.
     */
    @Override
    public List<ReviewEntity> getTopReviewsOfHotel(int hotelId, String queryString, int topCount) throws Exception {
        throw new NotImplementedException();
    }
}
