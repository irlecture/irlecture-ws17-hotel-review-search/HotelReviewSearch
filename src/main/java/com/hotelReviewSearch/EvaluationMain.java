package com.hotelReviewSearch;

import com.hotelReviewSearch.Dao.SearchDaoInterface;
import com.hotelReviewSearch.Dao.SearchEngineHotelReviewsDao;
import com.hotelReviewSearch.Entity.EvaluationRelevance;
import com.hotelReviewSearch.Entity.EvaluationTopic;
import com.hotelReviewSearch.Evaluation.EvaluationEngine;
import com.hotelReviewSearch.Evaluation.FeedbackMissingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Main class for evaluation tasks
 */
public class EvaluationMain
{
    private static final String topicsPath = "data/evaluation/topics";
    private static final String relevancesPath = "data/evaluation/relevances";

    private static final Logger logger = LogManager.getLogger(EvaluationMain.class);

    /**
     * Public main method to trigger the evaluation process
     *
     * @param args arguments
     */
    public static void main(String[] args)
    {
        DecimalFormat decimalFormat = new DecimalFormat("#.#####");
        /* CHANGE THE DAO HERE IF NECESSARY */
        SearchDaoInterface searchDao = new SearchEngineHotelReviewsDao();

        EvaluationEngine evaluationEngine = new EvaluationEngine(searchDao);
        try {
            // 1. get all topics
            List<EvaluationTopic> topics = evaluationEngine.getTopicsFromJsonFiles(topicsPath);
            logger.info("[" + topics.size() + "] topics read.");

            double summarizedTopicPrecisions = 0;
            double summarizedTopicRecalls = 0;
            for (EvaluationTopic topic : topics) {
                logger.debug("Search reviews for topic [" + topic.getId() + "].");

                // 2. for each topic, make a search request
                List<EvaluationRelevance> evaluationRelevances = evaluationEngine.getRelevanceTemplatesByTopic(topic);
                logger.debug("Retrieved [" + evaluationRelevances.size() + "] hotel-review-pairs for topic [" +
                        topic.getId() + "].");

                // 3. Fill the topic with relevance templates
                topic.setRelevances(evaluationRelevances);

                String relevanceFileName = relevancesPath + "/relevance_" + topic.getId() + ".json";
                File file = new File(relevanceFileName);

                if (file.exists()) {
                    logger.debug("Found relevance file [" + relevanceFileName + "] for topic.");
                    // 3.5 Update the relevance file
                    if (evaluationEngine.updateRelevanceFile(topic, relevanceFileName)) {
                        logger.warn("Relevance file [" + relevanceFileName + "] was updated. Please give necessary feedback.");
                        continue;
                    }
                    try {
                        // 4. Calculate Efficiency
                        double topicPrecision = evaluationEngine.getAveragePrecisionOfTopic(topic, relevanceFileName);
                        double topicRecall = evaluationEngine.getAverageRecallOfTopic(topic, relevanceFileName);

                        logger.info("Efficiency of topic [" + topic.getId() + "] : Precision [" +
                                decimalFormat.format(topicPrecision) + "] and Recall [" +
                                decimalFormat.format(topicRecall) + "]");

                        summarizedTopicPrecisions += topicPrecision;
                        summarizedTopicRecalls += topicRecall;
                    } catch (FeedbackMissingException exception) {
                        logger.warn(exception.getMessage());
                    }

                } else {
                    // 4. Write out the topics with relevance, (don't overwrite if file exists)
                    evaluationEngine.writeRelevances(topic, relevancesPath, false);
                }
            }

            // Calculate the mean average precision fo all topics
            double meanAveragePrecision = summarizedTopicPrecisions / topics.size();
            logger.info("Mean average PRECISION of all topics is: " + decimalFormat.format(meanAveragePrecision));

            // Calculate the mean average recall fo all topics
            double meanAverageRecall = summarizedTopicRecalls / topics.size();
            logger.info("Mean average RECALL of all topics is: " + decimalFormat.format(meanAveragePrecision));

        } catch (Exception e) {
            logger.error("Error while evaluation process: " + e.getMessage());
        }
        logger.info("Close evaluation system now. Goodbye!");
    }
}
