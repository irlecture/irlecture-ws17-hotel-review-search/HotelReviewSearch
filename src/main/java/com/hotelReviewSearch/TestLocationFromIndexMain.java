package com.hotelReviewSearch;

import com.hotelReviewSearch.Dao.SearchEngineHotelReviewsDao;
import com.hotelReviewSearch.Entity.EvaluationTopic;
import com.hotelReviewSearch.Evaluation.EvaluationEngine;
import com.hotelReviewSearch.Helper.LocationQuery;
import org.apache.lucene.queryparser.classic.ParseException;

import java.io.IOException;
import java.util.List;

public class TestLocationFromIndexMain {

    private static final String topicsPath = "data/evaluation/topics";

    public static void main(String[] args) throws IOException, ParseException {

        SearchEngineHotelReviewsDao searchDao = new SearchEngineHotelReviewsDao();
        EvaluationEngine evaluationEngine = new EvaluationEngine(searchDao);
        List<EvaluationTopic> topics = evaluationEngine.getTopicsFromJsonFiles(topicsPath);

        for (EvaluationTopic topic: topics) {
            System.out.println(topic.getTitle());
            LocationQuery locationQuery = searchDao.getLocationsInQuery(topic.getTitle());
            System.out.println("resultLocation: " + locationQuery.getLocation().toString());
            System.out.println("resultQuery: " + locationQuery.getEditedQueryString());
            System.out.println("--------------");
        }
    }
}
