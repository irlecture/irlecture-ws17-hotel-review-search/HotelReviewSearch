package com.hotelReviewSearch.Controller;

import com.hotelReviewSearch.Service.SuggestionService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

/**
 * REST-API Controller for the suggestions of the input
 */
@RestController
public class SuggestionController {

    private static final Logger logger = LogManager.getLogger(SuggestionController.class);


    @Autowired
    private SuggestionService suggestionService;

    /**
     * Return suggestions for the passed query.
     * @param query The query to make suggestions for.
     * @return String-Array of suggestions.
     */
    @RequestMapping(value= "/REST/suggest", method = RequestMethod.GET)
    public String[] getAllStations(@RequestParam(value = "q") String query,
                                   @RequestParam(value = "e", required = false, defaultValue = "queryLog") String engine){

        try {
            return this.suggestionService.getSuggestions(query, engine);
        } catch (Exception e) {
            logger.error("Error while retrieving suggestions:");
            logger.error(e.getMessage());
            return new String[0];
        }
    }
}
