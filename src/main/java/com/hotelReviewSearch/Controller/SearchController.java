package com.hotelReviewSearch.Controller;

import com.hotelReviewSearch.Entity.Page;
import com.hotelReviewSearch.Entity.SearchResult;
import com.hotelReviewSearch.Service.SearchService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collection;

/**
 * Servlet-Controller for the search requests.
 */
@Controller
public class SearchController {

    @Autowired
    private SearchService searchService;
    private static final Logger logger = LogManager.getLogger(SearchController.class);

    /**
     * Show search results for the given query.
     * @param q Query: the search query.
     * @param p Page: the requests page of the search results (default = 1).
     * @param s Size: the number of items per page.
     * @param model Thymeleaf model for dynamic content.
     * @return prepared thymeleaf template "search"
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String search(@RequestParam(value = "q") String q,
                         @RequestParam(value = "p", required = false, defaultValue = "1") int p,
                         @RequestParam(value = "s", required = false, defaultValue = "10") int s,
                         Model model){

        StopWatch sw = new StopWatch();
        sw.start();
        // if search string is empty stay at index page
        if (q.isEmpty()){
            return "index";
        }

        Page<SearchResult> searchResultPage;

        try {
            // toDo: estimate Number of Search Results
            searchResultPage = this.searchService.executeSearch(q, p, s);
        }
        catch (Exception ex) {
            logger.error(ex.getMessage());
            //todo: show a error page
            return null;
        }

        sw.stop();

        model.addAttribute("page", searchResultPage);
        model.addAttribute("query", q);
        model.addAttribute("time", sw.getTotalTimeSeconds());
        return "search";            // name of the html template page
    }
}
