package com.hotelReviewSearch.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller class for main search site
 */
@Controller
public class MainController
{
    /**
     * Index main page.
     * @return the name of the template
     */
    @RequestMapping("/")
    public String index() {
        return "index";
    }

}
