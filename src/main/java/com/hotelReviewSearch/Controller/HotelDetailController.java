package com.hotelReviewSearch.Controller;

import com.hotelReviewSearch.Entity.Hotel;
import com.hotelReviewSearch.Entity.ReviewEntity;
import com.hotelReviewSearch.Service.HotelDetailService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Servlet Controller for the Hotel-Detail Page.
 */
@Controller
public class HotelDetailController
{
    @Autowired
    private HotelDetailService hotelDetailService;

    private static final Logger logger = LogManager.getLogger(HotelDetailController.class);

    /**
     * Show hotel details for the requested hotelid.
     * @param hotelid ID of the hotel to show details of.
     * @param query the query string
     * @param model Thymeleaf model for dynamic content.
     * @return prepared thymeleaf template "detail"
     */
    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public String detail(@RequestParam(value = "hotelid") Integer hotelid,
                         @RequestParam(value = "q") String query,
                         Model model){

        if (hotelid == null){
            return "Keine HotelId uebergeben.";
        }

        Hotel hotel;
        try {
            hotel = hotelDetailService.getHotel(hotelid, query);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return "Fehler beim Einlesen des Hotels.";
        }
        model.addAttribute("hotel", hotel);
        model.addAttribute("query", query);

        return "detail";
    }
}
