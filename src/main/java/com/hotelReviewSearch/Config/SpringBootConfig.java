package com.hotelReviewSearch.Config;

import com.hotelReviewSearch.Interceptor.RequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Spring boot configuration class
 */
@Configuration
@EnableAsync
public class SpringBootConfig extends WebMvcConfigurerAdapter {

    @Autowired
    RequestInterceptor requestInterceptor;

    /**
     * Method to add the interceptor to all controller actions for the path "/search"
     *
     * @param registry the InterceptorRegistry object to use
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(requestInterceptor).addPathPatterns("/search");
    }
}
