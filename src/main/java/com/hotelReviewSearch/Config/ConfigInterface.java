package com.hotelReviewSearch.Config;

import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.Similarity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

public interface ConfigInterface {

    /** Declaration of index fields for indexing and searching **/
    String HOTEL_NAME = "name";
    String HOTEL_DESCRIPTION = "description";
    String HOTEL_ID = "hotelId";
    String HOTEL_ID_POINT = "hotelIdPoint";
    String HOTEL_CITY = "city";
    String HOTEL_COUNTRY = "country";
    String HOTEL_PROVINCE = "province";
    String HOTEL_REGION = "region";
    String HOTEL_CONTINENT = "continent";
    String REVIEW_TITLE = "title";
    String REVIEW_TEXT = "text";
    String REVIEWS_ARRAY = "reviews";
    String REVIEW_ID = "reviewId";
    String JSON_FILE = "jsonFile";
    String SEARCH_REQUEST = "searchRequest";

    /**
     * Defines the interval in wich SuggestionRefreshDaemon get's executed
     */
    int REFRESH_SUGGESTION_TIME = 30;
    TimeUnit REFRESH_SUGGESTION_TIMEUNIT = TimeUnit.SECONDS;

    /**
     * The standard directory where lucene creates the index
     */
    String indexDirectory = "data/lucene_index";

    /**
     * The directory where lucene creates the index for each Review
     */
    String reviewIndexDirectory = "data/lucene_review_index";

    /**
     * The directory where lucene creates the index for each Review
     */
    String hotelIndexDirectory = "data/lucene_hotel_index";

    /**
     * The directory where lucene creates the index for each Review
     */
    String hotelReviewIndexDirectory = "data/lucene_hotelreview_index";

    /**
     * The directory where lucene creates the index for each request
     */
    String requestIndexDirectory = "data/lucene_request_index";

    /**
     * The path to the source reviews in the data directory
     */
    String reviewPath = "data/normalized";

    /**
     * The path of the searchRequestsLog
     */
    String searchRequestLogPath = "log/searchRequests.log";

    /**
     * Custom StopWords to be used in an Analyzer for searching hotel reviews.
     */
    Collection<String> customHotelReviewSearchStopWords = new ArrayList<>(Arrays.asList(
            "hotel"));

    /**
     * Custom StopWords to be used in an Analyzer for searching locations.
     */
    Collection<String> customLocationSearchStopWords = new ArrayList<>(Arrays.asList(
            "nice", "best"));

    /**
     * Custom StopWords to be used on the query nGrams for the backwards search for found locations in the query.
     */
    Collection<String> locationBackwardsSearchFilter = new ArrayList<>(Arrays.asList(
            "new", "to"));

    /**
     * UnBoost-Factor for location terms in a query.
     * e.g. 0.1 = weight of location terms in the text is 10%
     * => but these terms get searched explicitly in the location fields
     */
    String locationTermPunishment = "0.1";

    /**
     * Boost-Factor for location queries in a boolean query,
     * where location terms are punished in the textQuery.
     */
    float locationQueryBoost = 3;

    /**
     * Enables usage of a custom stopWord list to improve the quality of the search results.
     */
    boolean customStopWordListActivated = true;

    /**
     * Enables Sentimental Analysis to improve the quality of the search results.
     */
    boolean sentimentalAnalysisActivated = true;

    /**
     * Enables qSell to improve the quality of the search results.
     */
    boolean qSpellActivated = true;

    /**
     * Enables Lemmatisation to improve the quality of the search results.
     */
    boolean lemmatisationActivated = true;

    /**
     * Enables Query-Pre-Processing: Extract the locations in the query string.
     * UnBoost the locations in the query and search the locations in the explicit fields.
     */
    boolean locationSearchActicated = true;

    /**
     * Define here the retrieval model, standard lucene: BM25Similarity
     * Note that you have to re run indexing process after changing
     */
    Similarity retrievalModel = new BM25Similarity();
}
