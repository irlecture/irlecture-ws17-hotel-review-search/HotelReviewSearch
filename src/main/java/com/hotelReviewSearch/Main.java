package com.hotelReviewSearch;

import com.hotelReviewSearch.Config.ConfigInterface;
import com.hotelReviewSearch.Daemon.SuggestionRefreshDaemon;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@SpringBootApplication
public class Main implements ConfigInterface {

    public static void main(String[] args) {

        SpringApplication.run(Main.class, args);
    }

}
