package com.hotelReviewSearch.Interceptor;

import com.hotelReviewSearch.Entity.SearchRequest;
import com.hotelReviewSearch.Helper.LogHelper;
import com.hotelReviewSearch.Index.RequestIndexEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Request interceptor class to define logic before and after a request
 */
@Component
public class RequestInterceptor extends HandlerInterceptorAdapter {

    private SearchRequest searchRequest;
    @Autowired
    private RequestIndexEngine requestIndexEngine;

    /**
     * PreHandle function will be called before the search controller runs its logic
     *
     * @param request the HttpServletRequest
     * @param response the HttpServletResponse
     * @param object the handler object
     * @return true if everything is fine
     * @throws Exception if something went wrong
     */
    @Override
    public boolean preHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object object
    ) throws Exception {
        // Get the query parameter and create a new search request object for logging purposes
        String query = ServletRequestUtils.getStringParameter(request, "q");
        int page = ServletRequestUtils.getIntParameter(request, "p", 1);
        int itemsPerPage = ServletRequestUtils.getIntParameter(request, "s", 5);
        searchRequest = new SearchRequest(query, page, itemsPerPage);
        searchRequest.setRemoteAddress(request.getRemoteAddr());
        searchRequest.setRequestUrl(request.getRequestURI());
        return true;
    }

    /**
     * PostHandle function will be called after the search controller runs its logic
     *
     * @param request the HttpServletRequest
     * @param response the HttpServletResponse
     * @param object the handler object
     * @param model the view model
     * @throws Exception if something went wrong
     */
    @Override
    public void postHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object object,
            ModelAndView model
    ) throws Exception {
        // Write the captured request to log file
        LogHelper.writeRequestToLog(searchRequest);
        // Write the captured request to the QueryIndex
        requestIndexEngine.appendQueryAsync(searchRequest.getSearchString());
        System.out.println("started async call of appending query to index on thread: " + Thread.currentThread().getName());
    }
}
