package com.hotelReviewSearch.Service;

import com.hotelReviewSearch.Dao.SearchDaoInterface;
import com.hotelReviewSearch.Entity.Page;
import com.hotelReviewSearch.Entity.SearchResult;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.Collection;

@Service
public class SearchService {

    @Autowired
    @Qualifier("liveHotelReviewData")
    private SearchDaoInterface searchDaoInterface;

    public Page<SearchResult> executeSearch(String query, int page, int pageSize) throws IOException, ParseException, InvalidTokenOffsetsException {

        try {

            return this.searchDaoInterface.getSearchResultsPage(query, page, pageSize);
        } catch (IOException | InvalidTokenOffsetsException | ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

}
