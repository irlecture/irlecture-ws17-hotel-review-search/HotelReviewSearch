package com.hotelReviewSearch.Service;

import com.hotelReviewSearch.Dao.HotelDaoInterface;
import com.hotelReviewSearch.Entity.Hotel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class HotelDetailService {

    @Autowired
    @Qualifier("liveHotelReviewData")
    private HotelDaoInterface hotelDao;

    public Hotel getHotel(int hotelId, String queryString) throws Exception {
        return hotelDao.getHotelWithTopReviewsById(hotelId, queryString, 10);
    }

}
