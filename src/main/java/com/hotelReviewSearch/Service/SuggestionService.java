package com.hotelReviewSearch.Service;

import com.hotelReviewSearch.Config.ConfigInterface;
import com.hotelReviewSearch.Daemon.SuggestionRefreshDaemon;
import com.hotelReviewSearch.Dao.SuggestionDaoInterface;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Service
public class SuggestionService implements ConfigInterface, InitializingBean{

    @Autowired
    @Qualifier("suggestOnTermFrequency")
    private SuggestionDaoInterface termFrequencySuggestionEngine;

    @Autowired
    @Qualifier("suggestOnQueryLog")
    private SuggestionDaoInterface queryLogSuggestionEngine;

    private ScheduledExecutorService suggestionRefreshSheduler;

    public String[] getSuggestions(String query, String engine) throws Exception {

        if (engine == null || engine.isEmpty()){
            throw new Exception("Suggestions requested but no engine defined.");
        }
        if (engine.equals("queryLog")){
            return this.queryLogSuggestionEngine.getSuggestions(query);

        } else if (engine.equals("termFreq")){
            return this.termFrequencySuggestionEngine.getSuggestions(query);
        } else {
            throw new Exception("Suggestion requested with unknown SuggestionEngine: " +engine);
        }
    }

    /**
     * Get's called after autowired attributes are set.
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        // the scheduler needs a reference to the used queryLogSuggestionEngine
        // so this is the right time to start it
        startSuggestionRefreshScheduler();
    }

    /**
     * Starts the scheduler for the suggestion refreshing
     * so that new queries are recognised
     */
    private void startSuggestionRefreshScheduler(){
        // create a threadPool, containing just one tread
        if (suggestionRefreshSheduler == null){
            suggestionRefreshSheduler = Executors.newScheduledThreadPool(1);

            // execute suggestion refreshing each X timeunits with a startup delay of X timeunits
            suggestionRefreshSheduler.scheduleAtFixedRate(new SuggestionRefreshDaemon(queryLogSuggestionEngine),
                    REFRESH_SUGGESTION_TIME,
                    REFRESH_SUGGESTION_TIME,
                    REFRESH_SUGGESTION_TIMEUNIT);
        }
    }
}
