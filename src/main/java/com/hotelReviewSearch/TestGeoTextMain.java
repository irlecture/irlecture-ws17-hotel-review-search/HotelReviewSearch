package com.hotelReviewSearch;

import com.hotelReviewSearch.Dao.SearchDaoInterface;
import com.hotelReviewSearch.Dao.SearchEngineHotelReviewsDao;
import com.hotelReviewSearch.Entity.EvaluationTopic;
import com.hotelReviewSearch.Evaluation.EvaluationEngine;
import org.apache.commons.lang3.text.WordUtils;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class TestGeoTextMain {

    private static final String pythonScript = "src/main/python/find_geo_locations/geo_locations.py";
    private static final String topicsPath = "data/evaluation/topics";
    private static final String pythonPath = "C:/Program Files/Python36/python.exe";

    public static void main(String[] args) throws IOException {

        SearchDaoInterface searchDao = new SearchEngineHotelReviewsDao();
        EvaluationEngine evaluationEngine = new EvaluationEngine(searchDao);
        List<EvaluationTopic> topics = evaluationEngine.getTopicsFromJsonFiles(topicsPath);

        String pythonFileFullPath = new File(pythonScript).getAbsolutePath();

        for (EvaluationTopic topic: topics) {
            String capitalized = WordUtils.capitalize(topic.getTitle());
            System.out.println(capitalized);
            ProcessBuilder pb = new ProcessBuilder(Arrays.asList(pythonPath, pythonFileFullPath, "-q", capitalized, "--cities", "--countries"));
            Process p = pb.start();

            BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String ret = in.readLine();
            System.out.println(ret);
        }
    }
}
