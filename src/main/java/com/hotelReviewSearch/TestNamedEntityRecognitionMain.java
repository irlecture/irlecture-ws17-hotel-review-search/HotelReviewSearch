package com.hotelReviewSearch;

import com.hotelReviewSearch.Config.ConfigInterface;
import com.hotelReviewSearch.Dao.SearchDaoInterface;
import com.hotelReviewSearch.Dao.SearchEngineHotelReviewsDao;
import com.hotelReviewSearch.Entity.EvaluationTopic;
import com.hotelReviewSearch.Evaluation.EvaluationEngine;
import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.util.Triple;
import org.apache.commons.lang3.text.WordUtils;

import java.io.IOException;
import java.util.List;

public class TestNamedEntityRecognitionMain {

    private static final String topicsPath = "data/evaluation/topics";

    /**
     * Testing the Stanford NER on our Topics to detect locations. To make it work, download this:
     * https://nlp.stanford.edu/software/CRF-NER.shtml#Download
     * and copy files from classifiers folder to data/ner_classifiers
     * @param args
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        //String serializedClassifier = "data/ner_classifiers/english.all.3class.distsim.crf.ser.gz";
        //String serializedClassifier = "data/ner_classifiers/english.muc.7class.distsim.crf.ser.gz";

        // beste Ergebnisse hiermit:
        String serializedClassifier = "data/ner_classifiers/english.conll.4class.distsim.crf.ser.gz";

        AbstractSequenceClassifier<CoreLabel> classifier = CRFClassifier.getClassifier(serializedClassifier);


        SearchDaoInterface searchDao = new SearchEngineHotelReviewsDao();
        EvaluationEngine evaluationEngine = new EvaluationEngine(searchDao);
        List<EvaluationTopic> topics = evaluationEngine.getTopicsFromJsonFiles(topicsPath);

        for (EvaluationTopic topic: topics) {
            String query = topic.getTitle();
            System.out.println("----------------- normal query: ---------------------");
            System.out.println(query);
            System.out.print(classifier.classifyToString(query, "tsv", false));

            // Capitalize the first char of each term with 'Apache Commons Text'
            // because NER only finds locations when they are capitalized correctly
            String capitalized = WordUtils.capitalize(query);
            System.out.println("----------------- capitalized: ---------------------");
            System.out.println(capitalized);
            System.out.print(classifier.classifyToString(capitalized, "tsv", false));

            String upperCase = query.toUpperCase();
            System.out.println("----------------- full upper case: ---------------------");
            System.out.println(upperCase);
            System.out.print(classifier.classifyToString(upperCase, "tsv", false));
        }

    }
}
