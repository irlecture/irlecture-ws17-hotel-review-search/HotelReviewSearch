package com.hotelReviewSearch.Index;

import com.hotelReviewSearch.Config.ConfigInterface;
import com.hotelReviewSearch.Helper.JsonReviewsParser;
import org.apache.logging.log4j.LogManager;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.omg.PortableInterceptor.LOCATION_FORWARD;

import java.io.IOException;

public class HotelReviewIndexEngine extends AbstractIndexEngine implements ConfigInterface {

    public static final String JSON_LOCATION = "location";

    /**
     * Constructor
     */
    public HotelReviewIndexEngine() {
        this.indexPath = hotelReviewIndexDirectory;
        this.logger = LogManager.getLogger(HotelIndexEngine.class);
    }

    /**
     * Add all documents to the index for a single json file
     *
     * @param indexWriter the lucene index writer object
     * @param pathToJsonFile the path to the json file to index
     * @throws IOException in json object could not be parsed
     */
    @Override
    void addJsonFileDoc(IndexWriter indexWriter, String pathToJsonFile) throws IOException {
        JSONObject jsonObject = JsonReviewsParser.parseJsonObject(pathToJsonFile);
        if (jsonObject == null) {
            // simple return because the error will be logged in the helper class
            return;
        }

        String hotelName = (String) jsonObject.get(HOTEL_NAME);
        String hotelDescription = "", hotelCountry = "", hotelProvince = "",
                hotelRegion = "", hotelCity = "", hotelProposed_city = "", hotelContinent = "";

        hotelDescription = (String) jsonObject.get(HOTEL_DESCRIPTION);
        if (hotelDescription == null) {
            hotelDescription = "";
        }

        JSONObject location = (JSONObject) jsonObject.get(JSON_LOCATION);
        if (location != null){
            hotelCountry = (String) location.get(HOTEL_COUNTRY);
            hotelProvince = (String)  location.get(HOTEL_PROVINCE);
            hotelRegion = (String)  location.get(HOTEL_PROVINCE);
            hotelCity = (String) location.get(HOTEL_CITY);
            hotelContinent = (String) location.get(HOTEL_CONTINENT);
            hotelProposed_city = (String) location.get("proposed_city");
        }

        if (hotelCity == null){
            // if there is no city given by tripadvisor, take the city detected by geotext
            if (hotelProposed_city == null){
                hotelCity = "";
            }else {
                hotelCity = hotelProposed_city;
            }
        }
        if (hotelCountry == null) hotelCountry = "";
        if (hotelProvince == null) hotelProvince = "";
        if (hotelRegion == null) hotelRegion = "";
        if (hotelContinent == null) hotelContinent = "";

        int hotelId = hotelName.hashCode();

        Document hotelReviewDocument = new Document();
        hotelReviewDocument.add(new StoredField(HOTEL_ID, hotelId));
        hotelReviewDocument.add(new StoredField(JSON_FILE, pathToJsonFile));
        hotelReviewDocument.add(new IntPoint(HOTEL_ID_POINT, hotelId));
        hotelReviewDocument.add(new TextField(HOTEL_NAME, hotelName, Field.Store.YES));
        hotelReviewDocument.add(new TextField(HOTEL_DESCRIPTION, hotelDescription, Field.Store.YES));
        hotelReviewDocument.add(new TextField(HOTEL_CITY, hotelCity, Field.Store.YES));
        hotelReviewDocument.add(new TextField(HOTEL_COUNTRY, hotelCountry, Field.Store.YES));
        hotelReviewDocument.add(new TextField(HOTEL_PROVINCE, hotelProvince, Field.Store.YES));
        hotelReviewDocument.add(new TextField(HOTEL_REGION, hotelRegion, Field.Store.YES));
        hotelReviewDocument.add(new TextField(HOTEL_CONTINENT, hotelContinent, Field.Store.YES));

        JSONArray jsonArrayReviews = (JSONArray) jsonObject.get(REVIEWS_ARRAY);
        reviewConterPerHotel = 0;
        for (Object reviewObject: jsonArrayReviews) {
            JSONObject reviewJsonObject = (JSONObject) reviewObject;

            if (reviewJsonObject.containsKey(REVIEW_TEXT) && reviewJsonObject.containsKey(REVIEW_TITLE)) {

                hotelReviewDocument.add(new TextField(REVIEW_TEXT, (String) reviewJsonObject.get(REVIEW_TEXT), Field.Store.YES));
                hotelReviewDocument.add(new TextField(REVIEW_TITLE, (String) reviewJsonObject.get(REVIEW_TITLE), Field.Store.YES));
                reviewConterPerHotel++;
                reviewCounter++;
            } else {
                logger.warn("Can not add review to index because the fields [" + REVIEW_TEXT + "] and [" +
                        REVIEW_TITLE + "] are not available for hotel [" + hotelName + "].");
            }
        }

        indexWriter.addDocument(hotelReviewDocument);
        reviewsProHotel.put(hotelCounter,reviewConterPerHotel);
        hotelCounter++;
    }
}
