package com.hotelReviewSearch.Index;

import com.hotelReviewSearch.Config.ConfigInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.UpgradeIndexMergePolicy;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class RequestIndexEngine implements ConfigInterface{

    private Logger logger;
    private String indexPath;

    /**
     * Constructor
     */
    public RequestIndexEngine() {
        this.logger = LogManager.getLogger(RequestIndexEngine.class);
        this.indexPath = requestIndexDirectory;
    }

    /**
     * Creates the lucene index for search requests in the searchRequests.log
     *
     * @param resetIndex if set to true, index will be deleted before creating
     * @throws IOException if index directory is not accessible
     */
    public void createIndex(boolean resetIndex) throws IOException{
        logger.info("Create the lucene index for the searchRequests ...");

        // Create the lucene standard analyzer
        StandardAnalyzer analyzer = new StandardAnalyzer();

        // Create the index directory
        File indexDirectoryFile = new File(indexPath);
        Directory index = FSDirectory.open(indexDirectoryFile.toPath());

        // Create the index writer
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        IndexWriter indexWriter = new IndexWriter(index, config);

        // Delete all indices if the flag is set to true
        if (resetIndex) {
            logger.info("Reset the existing index ...");
            indexWriter.deleteAll();
        }

        // Get the queries from the searchRequestLog
        int lineCounter = 0;
        int queryCounter = 0;
        try{
            FileInputStream fstream = new FileInputStream(searchRequestLogPath);
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String strLine;

            while ((strLine = br.readLine()) != null)   {
                lineCounter ++;
                Pattern pattern = Pattern.compile("Query: \\[(.*)\\], Page: \\[");
                Matcher matcher = pattern.matcher(strLine);
                if (matcher.find())
                {
                    String query = "";
                    try {
                        query = matcher.group(1);
                        addQueryDoc(indexWriter, query);
                        queryCounter ++;
                    } catch (Exception ex){
                        logger.warn("Can not add query [" + query + "] to index. Skip this one. An exception was thrown: ");
                        logger.warn(ex.getMessage());
                    }

                } else {
                    logger.warn("No query found line [" + lineCounter + "] of searchRequestLog. The line is: " + strLine);
                }
            }
            fstream.close();
        } catch (IOException e) {
            logger.error("Can not read searchRequestLog. An exception was thrown:");
            logger.error(e.getMessage());
        }

        logger.info("Indexed [" + queryCounter + "] queries from searchRequestLog.");
        indexWriter.close();
    }

    private void addQueryDoc(IndexWriter indexWriter, String query) throws IOException {
        logger.info("Add query [" + query + "] to the searchRequest index ...");
        Document queryIndexDocument = new Document();
        queryIndexDocument.add(new StoredField(SEARCH_REQUEST, query));
        indexWriter.addDocument(queryIndexDocument);
    }

    @Async
    public void appendQueryAsync(String query){
        // Create the lucene standard analyzer
        StandardAnalyzer analyzer = new StandardAnalyzer();

        try {
            // Create the index directory
            File indexDirectoryFile = new File(indexPath);
            Directory index = FSDirectory.open(indexDirectoryFile.toPath());

            // Create the index writer
            IndexWriterConfig config = new IndexWriterConfig(analyzer);

            IndexWriter indexWriter = new IndexWriter(index, config);

            addQueryDoc(indexWriter, query);
            indexWriter.commit();

            indexWriter.close();

        } catch (IOException ex){
            logger.error("An IOException occured during async appending of query to the requestIndex:");
            logger.error(ex.getMessage());
        }
        System.out.println("finished async action of appending query to index on thread: " + Thread.currentThread().getName());
    }
}
