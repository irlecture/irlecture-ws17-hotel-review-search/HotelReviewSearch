package com.hotelReviewSearch.Index;

import com.hotelReviewSearch.Config.ConfigInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Abstract class for index engines
 */
abstract class AbstractIndexEngine implements ConfigInterface
{
    protected Logger logger = LogManager.getLogger(AbstractIndexEngine.class);

    /**
     * Index directory. Overwrite to specify an other one.
     */
    protected String indexPath = indexDirectory;

    protected int reviewCounter = 0;
    protected int hotelCounter = 0;
    protected int reviewConterPerHotel = 0;
    protected HashMap<Integer, Integer> reviewsProHotel = new HashMap<>();

    /**
     * Creates the lucene index for hotel entities in the given directory
     *
     * @param resetIndex if set to true, index will be deleted before creating
     * @throws IOException if index directory is not accessible
     */
    public void createIndex(boolean resetIndex) throws IOException
    {
        logger.info("Create the lucene index for the Reviews ...");

        // Create the lucene standard analyzer
        StandardAnalyzer analyzer = new StandardAnalyzer();

        // Create the index directory
        File indexDirectoryFile = new File(indexPath);
        Directory index = FSDirectory.open(indexDirectoryFile.toPath());

        // Create the index writer
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        config.setSimilarity(retrievalModel);
        IndexWriter indexWriter = new IndexWriter(index, config);

        // Delete all indices if the flag is set to true
        if (resetIndex) {
            logger.info("Reset the existing index ...");
            indexWriter.deleteAll();
        }
        // Get the json files
        File[] jsonFiles = new File(reviewPath).listFiles();
        for (File file : jsonFiles) {
            if (file.isFile()) {
                try {
                    logger.info("Add file [" + file.getName() + "] to the review index ...");
                    addJsonFileDoc(indexWriter, file.getPath());
                } catch (Exception exception) {
                    logger.warn("Can not add file [" + file.getName() + "] to index. Skip this file. An exception was thrown: ");
                    logger.warn(exception.getMessage());
                }
            }
        }

        logger.info("Indexed [" + jsonFiles.length + "] json files with [" + hotelCounter + "] hotels and [" +
                reviewCounter + "] reviews.");
        System.out.println(Arrays.asList(reviewsProHotel));

        BufferedWriter br = new BufferedWriter(new FileWriter("data/dataForOverView.csv"));
        StringBuilder sb = new StringBuilder();
        Iterator it = reviewsProHotel.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            sb.append(pair.getKey());
            sb.append(",");
            sb.append(pair.getValue());
            sb.append("\n");
        }
        br.write(sb.toString());
        br.close();
        indexWriter.close();
    }

    abstract void addJsonFileDoc(IndexWriter indexWriter, String pathToJsonFile) throws IOException;
}
