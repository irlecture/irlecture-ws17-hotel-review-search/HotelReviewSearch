package com.hotelReviewSearch.Index;

import com.hotelReviewSearch.Config.ConfigInterface;
import com.hotelReviewSearch.Helper.JsonReviewsParser;
import org.apache.logging.log4j.LogManager;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.json.simple.JSONObject;

import java.io.IOException;

/**
 * Class for hotel indexing tasks
 */
public class HotelIndexEngine extends AbstractIndexEngine implements ConfigInterface
{
    /**
     * Constructor
     */
    public HotelIndexEngine() {
        this.indexPath = hotelIndexDirectory;
        this.logger = LogManager.getLogger(HotelIndexEngine.class);
    }

    /**
     * Add all documents to the index for a single json file
     *
     * @param indexWriter the lucene index writer object
     * @param pathToJsonFile the path to the json file to index
     * @throws IOException in json object could not be parsed
     */
    @Override
    void addJsonFileDoc(IndexWriter indexWriter, String pathToJsonFile) throws IOException {
        JSONObject jsonObject = JsonReviewsParser.parseJsonObject(pathToJsonFile);
        if (jsonObject == null) {
            // simple return because the error will be logged in the helper class
            return;
        }

        String hotelName = (String) jsonObject.get(HOTEL_NAME);
        String hotelDescription = "";

        hotelDescription = (String) jsonObject.get(HOTEL_DESCRIPTION);
        if (hotelDescription == null) {
            hotelDescription = "";
        }

        int hotelId = hotelName.hashCode();

        Document hotelDocument = new Document();
        hotelDocument.add(new StoredField(HOTEL_ID, hotelId));
        hotelDocument.add(new StoredField(JSON_FILE, pathToJsonFile));
        hotelDocument.add(new IntPoint(HOTEL_ID_POINT, hotelId));
        hotelDocument.add(new TextField(HOTEL_NAME, hotelName, Field.Store.YES));
        hotelDocument.add(new TextField(HOTEL_DESCRIPTION, hotelDescription, Field.Store.YES));
        indexWriter.addDocument(hotelDocument);
        hotelCounter++;
    }
}
