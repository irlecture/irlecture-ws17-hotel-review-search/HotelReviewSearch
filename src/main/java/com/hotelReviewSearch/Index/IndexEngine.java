package com.hotelReviewSearch.Index;

import com.hotelReviewSearch.Config.ConfigInterface;
import com.hotelReviewSearch.Helper.JsonReviewsParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Set;

/**
 * Class for main indexing tasks
 */
public class IndexEngine implements ConfigInterface
{
    private static final Logger logger = LogManager.getLogger(IndexEngine.class);

    /**
     * Creates the lucene index in the given directory
     *
     * @param resetIndex if set to true, index will be deleted before creating
     * @throws IOException if index directory is not accessible
     */
    public void createIndex(boolean resetIndex) throws IOException
    {
        logger.info("Create the lucene index ...");

        // Create the lucene standard analyzer
        StandardAnalyzer analyzer = new StandardAnalyzer();

        // Create the index directory
        File indexDirectoryFile = new File(indexDirectory);
        Directory index = FSDirectory.open(indexDirectoryFile.toPath());

        // Create the index writer
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        IndexWriter indexWriter = new IndexWriter(index, config);

        // Delete all indices if the flag is set to true
        if (resetIndex) {
            logger.info("Reset the existing index ...");
            indexWriter.deleteAll();
        }

        // Get the json files
        File[] jsonFiles = new File(reviewPath).listFiles();
        for (File file : jsonFiles) {
            if (file.isFile()) {
                try {
                    logger.info("Add file [" + file.getName() + "] to the index ...");
                    addJsonFileDoc(indexWriter, file.getPath());
                } catch (Exception exception) {
                    logger.warn("Can not add file [" + file.getName() + "] to index. An exception was thrown: ");
                    logger.warn(exception.getMessage());
                    logger.warn("Skip file [" + file.getName() + "] ...");
                }

            }
        }

        logger.info("Indexed " + jsonFiles.length + " files.");
        indexWriter.close();
    }

    /**
     * Add all documents to the index for a single json file
     *
     * @param indexWriter the lucene index writer object
     * @param pathToJsonFile the path to the json file to index
     * @throws IOException in json object could not be parsed
     */
    private static void addJsonFileDoc(IndexWriter indexWriter, String pathToJsonFile) throws IOException {
        Document doc = new Document();
        JSONObject jsonObject = JsonReviewsParser.parseJsonObject(pathToJsonFile);

        for(String field : (Set<String>) jsonObject.keySet()){
            Class type = jsonObject.get(field).getClass();

            if(field.equals(HOTEL_NAME) || field.equals(HOTEL_DESCRIPTION)) {
                doc.add(new TextField(field, (String) jsonObject.get(field), Field.Store.YES));

            } else if (field.equals("reviews")){

                JSONArray jsonArrayReviews = (JSONArray) jsonObject.get(field);

                for (Object jo: jsonArrayReviews) {
                    JSONObject reviewJsonObject = (JSONObject) jo;

                    for(String reviewJsonObjectField : (Set<String>) reviewJsonObject.keySet()){
                        if (reviewJsonObjectField.equals(REVIEW_TEXT) || reviewJsonObjectField.equals(REVIEW_TITLE)) {
                            doc.add(new TextField(reviewJsonObjectField, (String) reviewJsonObject.get(reviewJsonObjectField), Field.Store.YES));
                        }
                    }
                }
            }
        }
        indexWriter.addDocument(doc);
    }
}
