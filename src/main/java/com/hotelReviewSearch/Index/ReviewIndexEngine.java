package com.hotelReviewSearch.Index;

import com.hotelReviewSearch.Config.ConfigInterface;
import com.hotelReviewSearch.Helper.JsonReviewsParser;
import org.apache.logging.log4j.LogManager;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.IOException;

/**
 * Class for main indexing tasks
 */
public class ReviewIndexEngine extends AbstractIndexEngine implements ConfigInterface
{
    /**
     * Constructor
     */
    public ReviewIndexEngine() {
        this.indexPath = reviewIndexDirectory;
        this.logger = LogManager.getLogger(ReviewIndexEngine.class);
    }

    /**
     * Add all documents to the index for a single json file
     *
     * @param indexWriter the lucene index writer object
     * @param pathToJsonFile the path to the json file to index
     * @throws IOException in json object could not be parsed
     */
    @Override
    protected void addJsonFileDoc(IndexWriter indexWriter, String pathToJsonFile) throws IOException {
        JSONObject jsonObject = JsonReviewsParser.parseJsonObject(pathToJsonFile);
        if (jsonObject == null) {
            // simple return because the error will be logged in the helper class
            return;
        }

        String hotelName = (String) jsonObject.get(HOTEL_NAME);
        int hotelId = hotelName.hashCode();

        JSONArray jsonArrayReviews = (JSONArray) jsonObject.get(REVIEWS_ARRAY);

        for (Object reviewObject: jsonArrayReviews) {
            JSONObject reviewJsonObject = (JSONObject) reviewObject;

            if (reviewJsonObject.containsKey(REVIEW_TEXT) && reviewJsonObject.containsKey(REVIEW_TITLE)) {
                Document reviewDocument = new Document();
                reviewDocument.add(new StoredField(HOTEL_ID, hotelId));
                reviewDocument.add(new IntPoint(HOTEL_ID_POINT, hotelId));
                reviewDocument.add(new TextField(REVIEW_TEXT, (String) reviewJsonObject.get(REVIEW_TEXT), Field.Store.YES));
                reviewDocument.add(new TextField(REVIEW_TITLE, (String) reviewJsonObject.get(REVIEW_TITLE), Field.Store.YES));
                reviewDocument.add(new StoredField(REVIEW_ID, String.valueOf(reviewJsonObject.get("id"))));
                indexWriter.addDocument(reviewDocument);
                reviewCounter++;
            } else {
                logger.warn("Can not add review to index because the fields [" + REVIEW_TEXT + "] and [" +
                        REVIEW_TITLE + "] are not available for hotel [" + hotelName + "].");
            }
        }
        hotelCounter++;
    }
}
