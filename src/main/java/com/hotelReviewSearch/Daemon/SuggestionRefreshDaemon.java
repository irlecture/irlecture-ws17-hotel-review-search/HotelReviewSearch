package com.hotelReviewSearch.Daemon;

import com.hotelReviewSearch.Dao.SuggestionDaoInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.search.suggest.analyzing.AnalyzingSuggester;

import java.io.IOException;

/**
 * Daemon that rebuilds the suggester of the used suggestionEngine
 */
public class SuggestionRefreshDaemon extends Thread {

    private static final Logger logger = LogManager.getLogger(SuggestionRefreshDaemon.class);

    private SuggestionDaoInterface queryLogSuggestionEngine;

    public SuggestionRefreshDaemon(SuggestionDaoInterface suggestionDaoInterface){
        // mark thread as daemon, so it does not prevent the JVM from exiting when the program finishes but the thread is still running.
        // the JVM will exit and kill the daemon when the "main threads" have died
        setDaemon(true);
        this.queryLogSuggestionEngine = suggestionDaoInterface;
    }

    @Override public void run() {

        try {
            AnalyzingSuggester suggester = queryLogSuggestionEngine.buildNewSuggestor();
            queryLogSuggestionEngine.setSuggester(suggester);
            logger.info("Refreshed suggestions successfully.");
        } catch (IOException e) {
            logger.error("An error occured while refreshing the suggester:");
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }
}
